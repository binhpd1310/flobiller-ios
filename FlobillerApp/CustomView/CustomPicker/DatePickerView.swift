//
//  DatePickerView.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/2/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit


class DatePickerView: UIView {
    var cancelBlock : (() -> Void)?
    var doneBlock : (() -> Void)?
    var dateChanged : (() -> Void)?
    class func initWithNib() -> DatePickerView? {
        if let naviView = Bundle.main.loadNibNamed("DatePickerView", owner: nil, options: nil)?[0] as? DatePickerView {

            return naviView
        }
        return nil
    }

    @IBOutlet weak var picker: UIDatePicker!

    @IBAction func datepickerDidChange(_ sender: Any) {
        if dateChanged != nil {
            dateChanged!()
        }
    }
    @IBAction func buttonCancelAction(_ sender: Any) {
        if cancelBlock != nil {
            cancelBlock!()
        }
    }
    

    @IBAction func buttonDoneAction(_ sender: Any) {
        if doneBlock != nil {
            doneBlock!()
        }
    }

}
