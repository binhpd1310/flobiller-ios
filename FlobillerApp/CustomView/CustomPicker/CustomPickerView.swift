//
//  CustomPickerView.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/2/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class CustomPickerView: UIView {

    var doneClosure : (() -> Void)?
    var cancelClosure : (() -> Void)?
    @IBOutlet weak var titlePicker: UILabel!
    @IBOutlet weak var picker: UIPickerView!
    @IBAction func cancelAction(_ sender: Any) {
        cancelClosure?()
    }
    @IBAction func doneAction(_ sender: Any) {

        doneClosure?()

    }
    class func initWithNib() -> CustomPickerView? {
        if let naviView = Bundle.main.loadNibNamed("CustomPickerView", owner: nil, options: nil)?[0] as? CustomPickerView {

            return naviView
        }
        return nil
    }


}
