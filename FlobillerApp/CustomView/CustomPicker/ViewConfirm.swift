//
//  ViewConfirm.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/14/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit


class ViewConfirm: UIView {



    @IBOutlet weak var constraintWidth: NSLayoutConstraint!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var buttonOK: UIButton!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var rightButton: UIButton!
    var actionClosure : (() -> ())?
    var rightActionClosure : (() -> ())?
    override func awakeFromNib() {
         super.awakeFromNib()

    }
    @IBAction func buttonOkAction(_ sender: Any) {
        actionClosure?()
    }
    @IBAction func rightButtonAction(_ sender: Any) {
        rightActionClosure?()
    }
    class func initWithNib() -> ViewConfirm? {
        if let naviView = Bundle.main.loadNibNamed("ViewConfirm", owner: nil, options: nil)?[0] as? ViewConfirm {
            naviView.constraintWidth.constant = 0
            return naviView
        }
        return nil
    }
    class func initWithButton(_ leftButtonTitle : String, rightButtonTitle : String) -> ViewConfirm? {
        if let naviView = Bundle.main.loadNibNamed("ViewConfirm", owner: nil, options: nil)?[0] as? ViewConfirm {
            naviView.constraintWidth.constant = 155
            naviView.buttonOK.setTitle(leftButtonTitle, for: .normal)
            naviView.rightButton.setTitle(rightButtonTitle, for: .normal)
            return naviView
        }
        return nil
    }


}
