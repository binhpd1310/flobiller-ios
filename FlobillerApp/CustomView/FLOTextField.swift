//
//  FLOTextField.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/20/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class FLOTextField: UITextField {
    
    
//    override func draw(_ rect: CGRect) {
//        
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 3.0
        self.leftViewMode = .always
        let padingView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 14, height: 0))
        self.leftView = padingView
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.init(red: 178.0/255, green: 178.0/255, blue: 178.0/255, alpha: 1).cgColor
    }
}
