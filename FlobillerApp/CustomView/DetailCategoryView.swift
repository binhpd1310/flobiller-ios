//
//  DetailCategoryView.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class DetailCategoryView: UITableViewCell {

    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!


}
