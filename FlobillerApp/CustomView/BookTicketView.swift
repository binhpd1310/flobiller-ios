//
//  BookTicketView.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 4/7/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class BookTicketView: UIView,UIPickerViewDelegate,UIPickerViewDataSource {

    var ticketGroup : [[EventTicket]]?
    @IBOutlet weak var dateButtonScrollView: UIScrollView!
    @IBOutlet weak var timeButtonScrollView: UIScrollView!
    @IBOutlet weak var picker: UIPickerView!
    var selectedTicket : EventTicket?

    @IBOutlet weak var numberTicker: UILabel!

    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!

    @IBOutlet weak var viewA: UIView!

    @IBOutlet weak var viewF: UIView!

    @IBOutlet weak var viewFull: UIView!

    @IBOutlet weak var totalLabel: UILabel!
    var arrayButtonHour = [UIButton]()
    var arrayButtonDay = [ButtonDate]()
    @IBOutlet weak var viewPrice: UIView!
    var indexDaySelected = 0

    func displayView() {
        viewA.layer.cornerRadius = 2
        viewF.layer.cornerRadius = 2
        viewFull.layer.cornerRadius = 2
        if ticketGroup == nil {
            return
        }
        if ticketGroup!.count == 0 {
            return
        }
        picker.dataSource = self
        picker.delegate = self

        picker.selectRow(0, inComponent: 0, animated: true)
        dateButtonScrollView.contentSize = CGSize.init(width: 80 * ticketGroup!.count + 20, height: 40)
        viewPrice.layer.cornerRadius = 4
        for i in 0..<ticketGroup!.count {

            let button = Bundle.main.loadNibNamed("BookTicketView", owner: self, options: nil)?[1] as! ButtonDate
            button.frame = CGRect.init(x: 20 + (i * 80), y: 0, width: 50, height: 50)
            button.tag = i
            button.button.addTarget(self, action: #selector(buttonDayClick(button:)), for: .touchUpInside)
            dateButtonScrollView.addSubview(button)
            arrayButtonDay.append(button)

            if let firstTicket = ticketGroup?[i][0] {

                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = dateFormatter.date(from: firstTicket.ticket_date ?? BLANK)

                dateFormatter.dateFormat = "EEE"
                if date != nil {
                    button.dayLabel.text = dateFormatter.string(from: date!)
                    dateFormatter.dateFormat = "dd MMM"
                    button.monthYearLabel.text = dateFormatter.string(from: date!)
                }


            }


        }
        if arrayButtonDay.count > 0 {
            buttonDayClick(button: arrayButtonDay[0].button)
        }
    }

    func checkDisplayButtonDay() {
        if arrayButtonDay.count > 1 {
            for button in arrayButtonDay {
                button.viewUnderLine.isHidden = !(button.tag == indexDaySelected)
            }
        }
    }
    func buttonDayClick(button: UIButton) {
        indexDaySelected = button.tag
        arrayButtonHour.removeAll()
        for i in 0..<ticketGroup![button.tag].count {

            let hourButton = UIButton.init(frame: CGRect.init(x: 20 + (i * 100), y: 10, width: 80, height: 40))
            timeButtonScrollView.addSubview(hourButton)

            if let str = ticketGroup![button.tag][i].ticket_time {
                hourButton.setTitle(str.substring(to: str.index(str.startIndex, offsetBy: 5)) , for: .normal)
            }
            hourButton.backgroundColor = UIColor.init(red: 72.0/255, green: 159.0/255, blue: 72.0/255, alpha: 1.0)
            hourButton.addTarget(self, action: #selector(buttonHourClick(button:)), for: .touchUpInside)
            hourButton.tag = i
            
            hourButton.layer.borderColor = UIColor.black.cgColor
            hourButton.layer.cornerRadius = 3
            if i == 0 {
                hourButton.layer.borderWidth = 1
            }
            arrayButtonHour.append(hourButton)

        }
        selectedTicket = ticketGroup![button.tag][0]
        picker.reloadComponent(0)

        showPriceWithNumberTickets(1)
        checkDisplayButtonDay()
        picker.selectRow(0, inComponent: 0, animated: true)
    }
    func buttonHourClick(button: UIButton) {
        button.layer.borderWidth = 1
        for tmpBtn in arrayButtonHour {
            if tmpBtn != button {
                button.layer.borderWidth = 0
            }
        }
        selectedTicket = ticketGroup![indexDaySelected][button.tag]

        picker.reloadComponent(0)
        showPriceWithNumberTickets(1)
        picker.selectRow(0, inComponent: 0, animated: true)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }


    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Int(selectedTicket?.quantity ?? "0") ?? 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(row + 1)
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        showPriceWithNumberTickets(row + 1)
    }
    func showPriceWithNumberTickets(_ number : Int) {
        numberTicker.text =  "\(number)"
        feeLabel.text = selectedTicket?.commission
        amountLabel.text = selectedTicket?.amount
        let amount = Double(selectedTicket!.amount!) ?? 0
        let commission = Double(selectedTicket!.commission!) ?? 0
        subtotalLabel.text = "\(amount + commission)"
        totalLabel.text =  "\(Double(number) * (amount + commission))"
    }

}
class ButtonDate : UIView {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var viewUnderLine: UIView!
    @IBOutlet weak var button: UIButton!
}
