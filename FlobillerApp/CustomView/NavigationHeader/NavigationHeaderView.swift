//
//  NavigationHeaderView.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/20/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class NavigationHeaderView: UIView {
    override func awakeFromNib() {
        buttonMenu.isHidden = !buttonBack.isHidden
    }

    @IBOutlet weak var buttonMenu: UIButton!
    var buttonbackClosure : ((Void) -> Void)?
    var buttonMenuClosure : ((Void) -> Void)?
    @IBOutlet weak var buttonBack: UIButton!

    @IBAction func buttonBackAction(_ sender: Any) {
        if buttonbackClosure != nil {
            buttonbackClosure!()
        }
    }
    @IBAction func buttonMenuAction(_ sender: Any) {
        if buttonMenuClosure != nil {
            buttonMenuClosure!()
        }
    }

}
