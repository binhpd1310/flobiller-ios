//
//  NavigationPresentedView.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/25/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class NavigationPrestedView: UIView {

    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonSave: UIButton!


    var buttonBackClousure : (() -> Void)?
    var rightButtonBarClousure : (() -> ())?
    @IBAction func buttonSaveAction(_ sender: Any) {
        if rightButtonBarClousure != nil {
            rightButtonBarClousure!()
        }
    }
    @IBOutlet weak var navigationTitle: UILabel!
    @IBAction func buttonBackAction(_ sender: Any) {
        if buttonBackClousure != nil {
            buttonBackClousure!()
        }
    }
    class func initFromNib() -> NavigationPrestedView? {
        return Bundle.main.loadNibNamed("NavigationPrestedView", owner: nil, options: nil)?[0] as? NavigationPrestedView         
    }

    class func initWithTitle(_ title : String) -> NavigationPrestedView? {
        if let naviView = Bundle.main.loadNibNamed("NavigationPrestedView", owner: nil, options: nil)?[0] as? NavigationPrestedView {
            naviView.navigationTitle.text = title
            return naviView
        }
        return nil
    }
}
