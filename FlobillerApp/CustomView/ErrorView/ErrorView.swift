//
//  ErrorView.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/21/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class ErrorView: UIView {

    @IBOutlet weak var labelError: UILabel!
    @IBOutlet weak var buttonTrylater: UIButton!
    var tryLaterClosure : (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        buttonTrylater.layer.cornerRadius = 4

    }

    @IBAction func buttonTrylaterAction(_ sender: Any) {
        tryLaterClosure?()

    }
    
}
