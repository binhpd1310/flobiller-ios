//
//  Constant.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import Foundation

let BLANK = ""

// MARK:- URL
let registerUrl = "https://flobiller.flocash.com/api/user"
let URL_LOGO_PAYBILLER = "https://flobiller.flocash.com/assets/images/upload/"
let MAIN_COLOR = UIColor.init(red: 0, green: 71.0/255, blue: 132.0/255, alpha: 1)

//Key
let authCodeKey = "rememberAuthCode"
