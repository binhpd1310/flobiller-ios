//
//  CommonUltilities.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import SQLite

class CommonUltilities: NSObject {
    
    static func isValidEmail(email:String) -> Bool {

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }

    static func getListCountry() -> [CountryModel] {

        let path = Bundle.main.path(forResource: "FloCash", ofType: "sqlite")
        let db  =  try! Connection(path!)
        let key = Expression<String>("key")
        let value = Expression<String>("value")
        let code = Expression<String>("code")

        let countriesArr = NSMutableArray()

        for user in try! db.prepare(Table("Countries")) {
            print("key: \(user[key]), value: \(user[value]), code: \(user[code])")
            let country = CountryModel()
            country.key = user[key]
            country.name = user[value]
            country.code = user[code]
            countriesArr.add(country)
            // id: 1, name: Optional("Alice"), email: alice@mac.com
        }
        return (countriesArr as NSArray ) as! [CountryModel]

    }
    static func showMessage(_ message : String, isError : Bool ,actionBlock : (() ->())?) {
        let popUp = ViewConfirm.initWithNib()
        popUp?.actionClosure = actionBlock
        popUp?.messageLabel.text = message
        if isError {
            popUp?.viewTop.backgroundColor = UIColor.init(red: 220.0/255, green: 113.0/255, blue: 128.0/255, alpha: 1)
        }
        KGModal.sharedInstance().closeButtonType = .none
        KGModal.sharedInstance().tapOutsideToDismiss = false
        KGModal.sharedInstance().show(withContentView: popUp, andAnimated: false)
    }
    static func showMessage(_ message : String, leftButtonTitle : String,rightButtonTitle : String ,isError : Bool ,actionBlock : (() ->())?, rightActionBlock : (() -> ())?) {
        let popUp = ViewConfirm.initWithButton(leftButtonTitle, rightButtonTitle: rightButtonTitle)
        popUp?.actionClosure = actionBlock
        popUp?.rightActionClosure = rightActionBlock
        popUp?.messageLabel.text = message
        if isError {
            popUp?.viewTop.backgroundColor = UIColor.init(red: 220.0/255, green: 113.0/255, blue: 128.0/255, alpha: 1)
        }
        KGModal.sharedInstance().closeButtonType = .none
        KGModal.sharedInstance().tapOutsideToDismiss = false
        KGModal.sharedInstance().show(withContentView: popUp, andAnimated: false)
    }
}

class DataHelper : NSObject {
    static func isDidLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: "FLOdidLogin")
    }

    static func didLogin() {
        UserDefaults.standard.set(true, forKey: "FLOdidLogin")
    }
    static func didLogout() {
        UserDefaults.standard.set(nil, forKey: authCodeKey)
        UserDefaults.standard.set(false, forKey: "FLOdidLogin")
    }

   

}
