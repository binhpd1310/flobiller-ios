//
//  FunctionBaseViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/2/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class FunctionBaseViewController: UIViewController {
    var naviTitle: String?
    var header : NavigationPrestedView?
    var rightButtonBarAction : (() -> Void)?
    var needDismiss = true


    var errorView : ErrorView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.addHeaderView()

        errorView = Bundle.main.loadNibNamed("ErrorView", owner: nil, options: nil)?[0] as? ErrorView
        errorView?.frame = CGRect.init(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height-64)
        errorView?.isHidden = true
        self.view.addSubview(errorView!)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addHeaderView() {
        if let headerView = NavigationPrestedView.initWithTitle(naviTitle ?? BLANK) {
            header = headerView
            header?.buttonSave.isHidden = true
            header?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 64)
            header?.buttonBackClousure = { () in
                if self.needDismiss {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    _ = self.navigationController?.popViewController(animated: true)
                }


            }
            header?.rightButtonBarClousure = {
                if self.rightButtonBarAction != nil {
                    self.rightButtonBarAction!()
                }
            }
            self.view.addSubview(headerView)
        }

    }
    func showErrorView(_ errorText : String, tryLaterClosure : @escaping () -> ()) {
        errorView?.labelError.text = errorText
        errorView?.tryLaterClosure = tryLaterClosure
        errorView?.isHidden = false

    }

    func hideErrorView() {
        errorView?.isHidden = true
    }


}
