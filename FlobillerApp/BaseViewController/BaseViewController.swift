//
//  BaseViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/20/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var header : NavigationHeaderView?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.addHeaderView()

        
    }
    func setHiddenButtonBack() {
        if header != nil {
            header?.buttonBack.isHidden = true
        }
    }

    func setShowButtonMenu() {
        header?.buttonMenu.isHidden = false
        setHiddenButtonBack()
    }

   

    func addHeaderView() {
        if let headerView = Bundle.main.loadNibNamed("NavigationHeaderView", owner: nil, options: nil)?[0] as? NavigationHeaderView {
            header = headerView
            header?.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 64)
            header?.buttonbackClosure = { () in
                _ = self.navigationController?.popViewController(animated: true)
            }
            header?.buttonMenuClosure = {
                self.view.endEditing(true)
                self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            }
            self.view.addSubview(headerView)
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
