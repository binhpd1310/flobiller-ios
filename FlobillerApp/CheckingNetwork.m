//
//  CheckingNetwork.m
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/16/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

#import "CheckingNetwork.h"

#import "FlobillerApp-Swift.h"

@implementation CheckingNetwork

+ (CheckingNetwork *)shareIntance {
    static CheckingNetwork *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
- (id)init {
    self = [super init];
    // Detech network connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChangeStatus:) name:kReachabilityChangedNotification object:nil];
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    self.networkStatus = [self.reachability currentReachabilityStatus];
    return self;
}
#pragma mark - Check network status
-(void)networkChangeStatus:(NSNotification*)notifyObject {
    self.networkStatus = (self.reachability.currentReachabilityStatus != NotReachable);

}
@end
