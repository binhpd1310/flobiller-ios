//
//  CheckingNetwork.h
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/16/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FlobillerFramework/FlocashService.h>


@interface CheckingNetwork : NSObject
@property (strong, nonatomic) Reachability *reachability;
@property (nonatomic) BOOL networkStatus;
+ (CheckingNetwork *)shareIntance;
@end
