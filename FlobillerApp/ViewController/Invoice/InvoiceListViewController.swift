//
//  InvoiceListViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/3/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class InvoiceListViewController: FunctionBaseViewController {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!

    var serviveId : String?
    var userInfor : AccountInformation?
    var invoiceArray : NSArray?
    var invoiceDisplayArray : NSArray?
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataFromApi()
        getUserAccountInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func segmentDidChange(_ sender: Any) {
        self.checkSegmentState()
        tableView.reloadData()
    }
    func checkSegmentState() {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            self.invoiceDisplayArray = self.invoiceArray
        case 1:
            self.invoiceDisplayArray = self.invoiceArray?.filter { ($0 as! InvoiceModel).flocash_status == "S" } as NSArray?
        default:
            self.invoiceDisplayArray = self.invoiceArray?.filter { ($0 as! InvoiceModel).flocash_status == BLANK } as NSArray?
        }
    }
    func getDataFromApi() {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getAllInvoices(serviveId ?? "0" , successClosure: { arrayResult in
            self.invoiceArray = arrayResult
            self.checkSegmentState()
            self.tableView.reloadData()
            self.view.hideActivityView()
        }) { 
            self.view.hideActivityView()
        }
        

    }
    func getUserAccountInfo() {
        APIConnect.getUserInfo(success: { accountInfo  in
            self.userInfor = accountInfo
        }) { 

        }
    }


}

extension InvoiceListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (invoiceDisplayArray == nil ) ? 0 : invoiceDisplayArray!.count

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "invoiceCellReuse") as? InvoiceListTableViewCell
        let invoiceTmp = invoiceDisplayArray![indexPath.row] as! InvoiceModel
        cell?.logoImageView.sd_setImage(with: URL(string: invoiceTmp.logo ?? BLANK))
        cell?.nameInvoiceLabel.text = invoiceTmp.name
        cell?.typeInvoiceLabel.text = "Bank Information"
        cell?.amountLabel.text = (invoiceTmp.currency ?? BLANK) + " " + (invoiceTmp.amount ?? BLANK)
        cell?.dateTimeLabel.text = invoiceTmp.generate_date
        if invoiceTmp.flocash_status != "S" && invoiceTmp.flocash_status != "P" {
            cell?.buttonView.isHidden = true
        } else {
            cell?.buttonView.isHidden = false
        }

        cell?.markAsPaidClosure = {
            self.markAsPaidOrderId(invoiceTmp.invoiceId!)
        }

        cell?.buttonViewClosure = {

            if self.userInfor == nil {
                APIConnect.getUserInfo(success: { accountInfo  in
                    self.userInfor = accountInfo
                    self.goToConfirmScreen(invoiceTmp)
                }) {
                    
                }

            } else {
                self.goToConfirmScreen(invoiceTmp)
            }
        }

        cell?.buttonPayNowClosure = {
            if self.userInfor == nil {
                APIConnect.getUserInfo(success: { accountInfo  in
                    self.userInfor = accountInfo
                    self.goToConfirmScreen(invoiceTmp)
                }) {

                }

            } else {
                self.goToConfirmScreen(invoiceTmp)
            }
        }

        return cell!

    }

    func markAsPaidOrderId(_ orderId : String) {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: BLANK)
        APIConnect.markAsPaid(orderId, successClosure: { msg in
            self.view.hideActivityView()
            CommonUltilities.showMessage(msg, isError: false, actionBlock: {
                self.getDataFromApi()
                KGModal.sharedInstance().hide(animated: true)
            })
        }) { 
            self.view.hideActivityView()
        }
    }

    func goToConfirmScreen(_ invoice : InvoiceModel) {
        let account = self.userInfor
        let storyboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
        let confirmVC = storyboard.instantiateViewController(withIdentifier: "payBillConfirm") as? PayBillConfirmViewController
        confirmVC?.accountNumber = (account?.countryCode ?? BLANK) + (account?.mobile ?? BLANK )
        confirmVC?.invoiceNo = invoice.invoiceId
        confirmVC?.accountName = (account?.firstName ?? BLANK) + " " + (account?.lastName ?? BLANK)
        confirmVC?.amount = invoice.amount
        confirmVC?.subcriptionName = invoice.name
        confirmVC?.subcriptionId = invoice.invoiceId
        confirmVC?.currency = invoice.currency
        confirmVC?.country = account?.country
        confirmVC?.email = account?.email
        confirmVC?.phoneCode = account?.countryCode
        confirmVC?.phoneNumber = account?.mobile
        confirmVC?.firstName = account?.firstName
        confirmVC?.lastName = account?.lastName
        self.navigationController?.pushViewController(confirmVC!, animated: true)
    }

}
