//
//  InvoiceListTableViewCell.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/3/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class InvoiceListTableViewCell: UITableViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameInvoiceLabel: UILabel!
    @IBOutlet weak var typeInvoiceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var buttonView: UIButton!
    var markAsPaidClosure : ((Void) -> Void)?

    var buttonViewClosure : ((Void) -> Void)?
    var buttonPayNowClosure : ((Void) -> Void)?


    override func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }

    @IBAction func markAsPaidButtonAction(_ sender: Any) {
        markAsPaidClosure?()
    }
    @IBAction func payNowButtonAction(_ sender: Any) {
        buttonPayNowClosure?()
    }
    @IBAction func viewButtonAction(_ sender: Any) {
        buttonViewClosure?()
    }
}
