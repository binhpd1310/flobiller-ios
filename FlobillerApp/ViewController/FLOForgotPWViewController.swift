//
//  FLOForgotPWViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/24/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class FLOForgotPWViewController: BaseViewController {

    @IBOutlet weak var emailTF: FLOTextField!
    override func viewDidLoad() {
        super.viewDidLoad()


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        if emailTF.text == BLANK {
            let toast = MDToast.init(text: "Please fill email!", duration: 0.5)
            toast.show()
            return
        }
        if !CommonUltilities.isValidEmail(email: emailTF.text!) {
            let toast = MDToast.init(text: "Email wrong format", duration: 0.5)
            toast.show()
            return
        }
        APIConnect.forgotPW(emailTF.text!) {
            let toast = MDToast.init(text: "Email send successfully", duration: 0.5)
            toast.show()
            _ = self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func buttonRegisAction(_ sender: Any) {
    }

    @IBAction func buttonBackToLoginAction(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
