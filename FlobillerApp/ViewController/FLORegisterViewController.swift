//
//  FLORegisterViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/20/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import SQLite


class FLORegisterViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate  {

    @IBOutlet weak var firstNameTF: FLOTextField!
    @IBOutlet weak var lastNameTF: FLOTextField!
    @IBOutlet weak var codeTF: FLOTextField!
    @IBOutlet weak var mobileTF: FLOTextField!
    @IBOutlet weak var emailTF: FLOTextField!
    @IBOutlet weak var passwordTF: FLOTextField!
    @IBOutlet weak var cPasswordTF: FLOTextField!
    @IBOutlet weak var countryNameTF: FLOTextField!

    @IBOutlet weak var pickerCountry: UIPickerView!
    //list contry code
    var countriesArray : [CountryModel]?

    var oldIndexSelect : Int = 0

    @IBOutlet weak var constrainBottom: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setHiddenButtonBack()
        countriesArray = CommonUltilities.getListCountry()
        pickerCountry.delegate = self
        pickerCountry.dataSource = self
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        codeTF.delegate = self
        mobileTF.delegate = self
        emailTF.delegate = self
        passwordTF.delegate = self
        cPasswordTF.delegate = self
        self.setDisplayForCountry()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setDisplayForCountry() {
        let selectedCountry = countriesArray?[oldIndexSelect]
        countryNameTF.text = selectedCountry?.name
        codeTF.text = selectedCountry?.code

    }

    //MARK: - Textfield delegate
    func hideAllKeyboard() {
        firstNameTF.resignFirstResponder()
        lastNameTF.resignFirstResponder()
        codeTF.resignFirstResponder()
        mobileTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        cPasswordTF.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //MARK: - Picker delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countriesArray?[row].name
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countriesArray!.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

    }

    @IBAction func registerButtonAction(_ sender: Any) {
        hideAllKeyboard()
        validateInput()
    }
    @IBAction func pickerCountryAction(_ sender: Any) {
        hideAllKeyboard()
        constrainBottom.constant = 0
        pickerCountry.selectRow(oldIndexSelect, inComponent: 0, animated: true)

    }

    @IBAction func cancelButtonAction(_ sender: Any) {
        constrainBottom.constant = -244
    }

    @IBAction func okButtonAction(_ sender: Any) {
        oldIndexSelect = pickerCountry.selectedRow(inComponent: 0)
        constrainBottom.constant = -244
        self.setDisplayForCountry()
    }

    func validateInput() {
        if firstNameTF.text == BLANK || lastNameTF.text == BLANK || mobileTF.text == BLANK || emailTF.text == BLANK || passwordTF.text == BLANK || cPasswordTF.text == BLANK{
            let toast = MDToast.init(text: "All fields must fill", duration: 0.5)
            toast.show()
            return
        }
        if !CommonUltilities.isValidEmail(email: emailTF.text!) {
            let toast = MDToast.init(text: "Email wrong format!", duration: 0.5)
            toast.show()
            return
        }
        if passwordTF.text!.characters.count <= 4 {
            let toast = MDToast.init(text: "Password too short!", duration: 0.5)
            toast.show()
            return
        }
        if passwordTF.text! != cPasswordTF.text! {
            let toast = MDToast.init(text: "Password is not match", duration: 0.5)
            toast.show()
            return
        }

        let input =  RegisterInput()
        input.email = emailTF.text
        input.country = countriesArray?[oldIndexSelect].key
        input.firstName = firstNameTF.text
        input.lastName = lastNameTF.text
        input.mobile = mobileTF.text
        input.passWord = passwordTF.text
        input.cPassWord = cPasswordTF.text


        APIConnect.register(input) { 
            let alert = UIAlertController.init(title: BLANK, message: "Register successfully", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                _ = self.navigationController?.popViewController(animated: true)
            })
            self.present(alert, animated: true, completion: nil)
        }
    }

}
