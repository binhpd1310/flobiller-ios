//
//  TutorialViewController.swift
//  FlobillerApp
//
//  Created by Monkeyyyy on 12/1/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class TutorialViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    var presentationPageIndex : Int = 0
    lazy var pageArr : [UIViewController] = {
        let first = UIStoryboard.init(name: "TutorialStoryboard", bundle: nil).instantiateViewController(withIdentifier: "firstVC") as! FirstTutorialViewController
        first.delegate = self
        let second = UIStoryboard.init(name: "TutorialStoryboard", bundle: nil).instantiateViewController(withIdentifier: "secondVC") as! SecondTutorialViewController
        second.delegate = self
        let third = UIStoryboard.init(name: "TutorialStoryboard", bundle: nil).instantiateViewController(withIdentifier: "thirdVC") as! ThirdTutorialViewController
        third.delegate = self
        return [first,second,third];
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "FLOdidShowTutorial")
        self.dataSource = self
        self.delegate = self
        if let firstVC = pageArr.first {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        let pageControl = UIPageControl.appearance()
        

        pageControl.backgroundColor = UIColor.init(red: 16.0/255, green: 135.0/255, blue: 200.0/255, alpha: 1.0)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @available(iOS 5.0, *)
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = pageArr.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = index - 1
        presentationPageIndex -= 1
        guard previousIndex >= 0 else {
            presentationPageIndex = 2
            return pageArr.last
        }
        return pageArr[previousIndex]
    }
    
    @available(iOS 5.0, *)
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = pageArr.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = index + 1
        presentationPageIndex += 1
        guard nextIndex != pageArr.count else {
            presentationPageIndex = 0
            return pageArr.first
        }
        return pageArr[nextIndex]
        
    }
    
    @available(iOS 6.0, *)
    public func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 3
    }// The number of items reflected in the page indicator.
    
    @available(iOS 6.0, *)
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return self.presentationPageIndex;
    }
    
    
    

}

extension TutorialViewController : TutorialDelegate {
    func skipButtonTapped() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let navi  =  storyboard.instantiateViewController(withIdentifier: "naviLogin")
        self.present(navi, animated: true) { 
            
        }
    }
}
