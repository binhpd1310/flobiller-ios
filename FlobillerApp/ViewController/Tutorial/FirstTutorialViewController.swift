//
//  FirstTutorialViewController.swift
//  FlobillerApp
//
//  Created by Monkeyyyy on 12/1/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

protocol TutorialDelegate {
    func skipButtonTapped() -> Void;
}
class FirstTutorialViewController: UIViewController {

    var delegate : TutorialDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonSkipTapped(_ sender: Any) {
        if let tmp = delegate {
            tmp.skipButtonTapped()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
