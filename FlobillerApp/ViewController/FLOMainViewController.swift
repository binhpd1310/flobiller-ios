//
//  FLOMainViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/24/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import GoogleSignIn

class FLOMainViewController: BaseViewController, GIDSignInUIDelegate {


    
    override func viewDidLoad() {
        super.viewDidLoad()
        setShowButtonMenu()
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    @IBAction func payBillButtonAction(_ sender: Any) {
        let payBillStoryboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
        let rootVC = payBillStoryboard.instantiateViewController(withIdentifier: "paybill") as? FunctionBaseViewController
        rootVC?.naviTitle = "PAY BILL"
        let navigationControler = UINavigationController.init(rootViewController: rootVC!)
        self.present(navigationControler, animated: true, completion: nil)

    }
    @IBAction func airTimeButtonAction(_ sender: Any) {
        let airTimeStoryboard = UIStoryboard.init(name: "AirTime", bundle: nil)
        let rootVC = airTimeStoryboard.instantiateViewController(withIdentifier: "operatorsList") as? FunctionBaseViewController
        rootVC?.naviTitle = "AIR TIME"
        let navigationControler = UINavigationController.init(rootViewController: rootVC!)
        self.present(navigationControler, animated: true, completion: nil)
    }

    @IBAction func giftCardButtonAction(_ sender: Any) {
        let giftCardStoryboard = UIStoryboard.init(name: "GiftCard", bundle: nil)
        let rootVC = giftCardStoryboard.instantiateViewController(withIdentifier: "giftCardListVC") as? FunctionBaseViewController
        rootVC?.naviTitle = "GIFT CARDS"
        let navigationControler = UINavigationController.init(rootViewController: rootVC!)
        self.present(navigationControler, animated: true, completion: nil)
    }

    @IBAction func callButtonAction(_ sender: Any) {
//        let ticketStoryboard = UIStoryboard.init(name: "Ticket", bundle: nil)
//        let rootVC = ticketStoryboard.instantiateViewController(withIdentifier: "ticketCategory") as? FunctionBaseViewController
//        rootVC?.naviTitle = "TICKETS"
//        let navigationControler = UINavigationController.init(rootViewController: rootVC!)
//        self.present(navigationControler, animated: true, completion: nil)
    }

    @IBAction func invoiceButtonAction(_ sender: Any) {
        let invoiceStoryboard = UIStoryboard.init(name: "Invoice", bundle: nil)
        let rootVC = invoiceStoryboard.instantiateViewController(withIdentifier: "InvoiceListVC") as? FunctionBaseViewController
        rootVC?.naviTitle = "INVOICES"
        let navigationControler = UINavigationController.init(rootViewController: rootVC!)
        self.present(navigationControler, animated: true, completion: nil)

    }
    @IBAction func subcriptionAction(_ sender: Any) {
//        let invoiceStoryboard = UIStoryboard.init(name: "Subcription", bundle: nil)
//        let rootVC = invoiceStoryboard.instantiateViewController(withIdentifier: "subscriptionVC") as? FunctionBaseViewController
//        rootVC?.naviTitle = "SUBCRIPTION"
//        let navigationControler = UINavigationController.init(rootViewController: rootVC!)
//        self.present(navigationControler, animated: true, completion: nil)
    }
    func googleLoginFunction() {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }

    
}
