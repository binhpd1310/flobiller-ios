//
//  SubscriptionDetailViewController.swift
//  FlobillerApp
//
//  Created by Lamdq on 8/30/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class SubscriptionDetailViewController: FunctionBaseViewController {

    var subcriptionDetail : UserSubcriptionModel?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var subscriptionLabel: UILabel!
    @IBOutlet weak var accountNameTF: UITextField!
    @IBOutlet weak var accountNumberTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscriptionLabel.text = subcriptionDetail!.billername
        if let url = URL.init(string: URL_LOGO_PAYBILLER + self.subcriptionDetail!.billerlogo!) {
            imageView.sd_setImage(with: url)
        }
        accountNameTF.text = subcriptionDetail!.account_name
        accountNumberTF.text = subcriptionDetail!.account_number
        self.header?.buttonSave.isHidden = false
        self.header?.rightButtonBarClousure = {
            self.view.endEditing(true)
            guard let name = self.accountNameTF.text, let number = self.accountNumberTF.text, name.characters.count != 0, number.characters.count != 0
                else {
                    CommonUltilities.showMessage("All Field must be fill!", isError: true, actionBlock: {
                        KGModal.sharedInstance().hide(animated: true)
                    })
                    return
            }
            APIConnect.updateSubscription(serviceId: (self.subcriptionDetail?.service_id)!, accname: name, accno: number, success: { 
                
            }, failure: { 
                
            })

            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func subscribeAction(_ sender: Any) {
        self.view.endEditing(true)
        
        
    }

    

}
