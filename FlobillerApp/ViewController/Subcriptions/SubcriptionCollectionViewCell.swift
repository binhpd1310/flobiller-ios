//
//  SubcriptionCollectionViewCell.swift
//  FlobillerApp
//
//  Created by Lamdq on 8/6/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class SubcriptionCollectionViewCell: UICollectionViewCell {
    
    var deleteSubcriptionBlock,viewInvoiceBlock, editBlock :(() -> ())?
    
    @IBOutlet weak var nameSubcriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBAction func deleteButtonAction(_ sender: Any) {
        if deleteSubcriptionBlock != nil {
            deleteSubcriptionBlock!()
        }
    }
   
    @IBAction func viewInvoiceAction(_ sender: Any) {
        if viewInvoiceBlock != nil {
            viewInvoiceBlock!()
        }
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
        if editBlock != nil {
            editBlock!()
        }
    }
}
