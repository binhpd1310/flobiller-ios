//
//  FLOSubcriptionsViewController.swift
//  FlobillerApp
//
//  Created by Lamdq on 8/6/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class FLOSubcriptionsViewController: FunctionBaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var userSubcriptionArray = [UserSubcriptionModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.isHidden = true
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getUserSubcriptions(success: { array in
            self.view.hideActivityView()

            self.userSubcriptionArray = array as! [UserSubcriptionModel]
            if self.userSubcriptionArray.count != 0 {
                self.collectionView.isHidden = false
                
            }
            self.collectionView.reloadData()
        }) {
            self.view.hideActivityView()

            self.collectionView.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension FLOSubcriptionsViewController : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionView.isHidden = !(self.userSubcriptionArray.count > 0);
        return self.userSubcriptionArray.count
    }
    
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subcriptionCellIdentifier", for: indexPath) as! SubcriptionCollectionViewCell
        cell.nameSubcriptionLabel.text = self.userSubcriptionArray[indexPath.row].billername
        if let url = URL.init(string: URL_LOGO_PAYBILLER + self.userSubcriptionArray[indexPath.row].billerlogo!) {
            cell.imageView.sd_setImage(with: url)
        }
        cell.viewInvoiceBlock = {
            let invoiceStoryboard = UIStoryboard.init(name: "Invoice", bundle: nil)
            let rootVC = invoiceStoryboard.instantiateViewController(withIdentifier: "InvoiceListVC") as? InvoiceListViewController
            rootVC?.naviTitle = "INVOICES"
            rootVC?.serviveId = self.userSubcriptionArray[indexPath.row].service_id
            
            self.navigationController?.pushViewController(rootVC!, animated: true);
        }
        cell.editBlock = {
            let invoiceStoryboard = UIStoryboard.init(name: "Subcription", bundle: nil)
            let rootVC = invoiceStoryboard.instantiateViewController(withIdentifier: "SubscriptionDetailVC") as? SubscriptionDetailViewController
            rootVC?.naviTitle = "SUBSCRIBE"
            rootVC?.subcriptionDetail = self.userSubcriptionArray[indexPath.row]
            let navigationControler = UINavigationController.init(rootViewController: rootVC!)
            self.present(navigationControler, animated: true, completion: nil)
        }
        
        cell.deleteSubcriptionBlock = {
            self.view.showActivityView(withLabel: "Loading")
            
            APIConnect.deleteUserSubscription(serviceId: self.userSubcriptionArray[indexPath.row].service_id ?? BLANK, success: { 
                self.userSubcriptionArray.remove(at: indexPath.row)
                self.view.hideActivityView()
                collectionView.reloadData()
                CommonUltilities.showMessage("Delete supscription successfully", isError: false, actionBlock: {
                    KGModal.sharedInstance().hide(animated: true)
                })
            }, failure: {
                self.view.hideActivityView()
                CommonUltilities.showMessage("Sever problem", isError: true, actionBlock: {
                    KGModal.sharedInstance().hide(animated: true)
                })
            })
        }
        
        cell.layer.cornerRadius = 3
        return cell
        
    }
    
    // MARK: - UICollectionViewFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let picDimension = self.view.frame.size.width / 2.0 - 10
        return CGSize(width: picDimension, height: picDimension * 1.2)
    }
    
}
