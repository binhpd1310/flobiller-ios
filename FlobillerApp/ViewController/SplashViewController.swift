//
//  SplashViewController.swift
//  FlobillerApp
//
//  Created by Monkeyyyy on 11/24/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var window2 : UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "gif_splash", withExtension: "gif")!)
        let advTimeGif = UIImage.sd_animatedGIF(with: imageData)
        imageView.image = advTimeGif
        self.imageView.animationRepeatCount = 1
        imageView.animationDuration = 2.5
        imageView.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.2) {
            
            self.imageView.image = UIImage.init(named: "gif_splash_last.png")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4) {
            self.window2 = UIWindow.init(frame: UIScreen.main.bounds)
            self.window2?.backgroundColor = .white;
            self.window2?.windowLevel = UIWindowLevelNormal;
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            var navi  =  storyboard.instantiateViewController(withIdentifier: "naviLogin")
            if !UserDefaults.standard.bool(forKey: "FLOdidShowTutorial") {
                 let tutorialStoryboard = UIStoryboard.init(name: "TutorialStoryboard", bundle: nil)
                 navi  = tutorialStoryboard.instantiateViewController(withIdentifier: "tutorial")
            }
            
            self.window2?.rootViewController =  navi
            self.window2?.makeKeyAndVisible()
            AppDelegate.shareIntance().windowLogin = self.window2
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
