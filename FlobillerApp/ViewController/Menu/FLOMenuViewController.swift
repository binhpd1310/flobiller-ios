//
//  FLOMenuViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/24/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FirebaseAuth
import FBSDKCoreKit
import FBSDKShareKit


class FLOMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,InviteDelegate, UIActionSheetDelegate, GIDSignInUIDelegate,FBSDKAppInviteDialogDelegate {



    @IBOutlet weak var imageview: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1    }
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "menuCellIdentifier") as? MenuTableViewCell {
            cell.lineTop.isHidden = true
            switch indexPath.row {
            case 0:
                cell.iconView.image = UIImage.init(named: "my_ticket.png")
                cell.titleLabel.text = "My Tickets"
            case 1:
                cell.iconView.image = UIImage.init(named: "ic_add_payment_card.png")
                cell.titleLabel.text = "Manage Payments"
            case 2:
                cell.iconView.image = UIImage.init(named: "ic_subcription.png.png")
                cell.titleLabel.text = "Subscriptions"
            case 3:
                cell.iconView.image = UIImage.init(named: "ic_edit.png")
                cell.titleLabel.text = "Biller"
            case 4:
                cell.iconView.image = UIImage.init(named: "ic_profile.png")
                cell.titleLabel.text = "Profile"
            case 5:
                cell.iconView.image = UIImage.init(named: "ic_profile_drawer.png")
                cell.titleLabel.text = "Invite Friends"
            case 6:
                cell.iconView.image = UIImage.init(named: "ic_about_drawer.png")
                cell.titleLabel.text = "About"
            case 7:
                cell.iconView.image = UIImage.init(named: "ic_about_drawer.png")
                cell.titleLabel.text = "Privacy"
            default:
                cell.iconView.image = UIImage.init(named: "ic_logout_drawer.png")
                cell.titleLabel.text = "Logout"
                cell.lineTop.isHidden = false
            }
            return cell
        }

        return UITableViewCell.init()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        var rootVC : FunctionBaseViewController?
        let storyboard = UIStoryboard.init(name: "Function", bundle: nil)
//        let payBillStoryboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
//        let airTimeStoryboard = UIStoryboard.init(name: "AirTime", bundle: nil)
//        let invoiceStoryboard = UIStoryboard.init(name: "Invoice", bundle: nil)
        switch indexPath.row {
        case 0:
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            return
        case 1:
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            return
        case 2:

            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            let subscriptionStoryboard = UIStoryboard.init(name: "Subcription", bundle: nil)
            rootVC = subscriptionStoryboard.instantiateViewController(withIdentifier: "subscriptionVC") as? FunctionBaseViewController
            rootVC?.naviTitle = "SUBCRIPTION"
        case 3:
            return
        case 4:
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            rootVC = storyboard.instantiateViewController(withIdentifier: "profileVC") as? FunctionBaseViewController
            rootVC?.naviTitle = "PROFILE"
        case 5:
            UIActionSheet.init(title: "Invite Friend", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Google","Facebook").show(in: self.view);
        case 6:
            //About Screen
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            return
        case 7 :
            //Privacy
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            return

        case 8:
            DataHelper.didLogout()
            self.menuContainerViewController.dismiss(animated: false, completion: nil)
            return
        default:
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            return

        }
        let navigationControler = UINavigationController.init(rootViewController: rootVC!)
        self.present(navigationControler, animated: true, completion: nil)

    }
    public func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        
        switch buttonIndex {
        case 2:
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.facebookAppInvite()
            }
            
            break
            
        default:
            if !AppDelegate.shareIntance().googleUserLogedIn {
                let centerNavi = self.menuContainerViewController.centerViewController as! UINavigationController
                let centerVC = centerNavi.viewControllers.first as! FLOMainViewController
                GIDSignIn.sharedInstance().delegate = self
                let when = DispatchTime.now() + 0.5
                DispatchQueue.main.asyncAfter(deadline: when) {
                    centerVC.googleLoginFunction()
                }
                
                
            } else {
                
                firebaseInviteAppViaGoogle()
            }
            
            
            
        }
    }
    
    func firebaseInviteAppViaGoogle() {
        
        if let invite = Invites.inviteDialog() {
            invite.setInviteDelegate(self)
            
            // NOTE: You must have the App Store ID set in your developer console project
            // in order for invitations to successfully be sent.
            
            // A message hint for the dialog. Note this manifests differently depending on the
            // received invitation type. For example, in an email invite this appears as the subject.
            
            invite.setMessage("Try this out!\n -\(GIDSignIn.sharedInstance().currentUser.profile.name)")
            // Title for the dialog, this is what the user sees before sending the invites.
            invite.setTitle("Invites Example")
            invite.setDeepLink("app_url")
            invite.setCallToActionText("Install!")
            invite.setCustomImage("https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png")
            invite.open()
        }

    }
    
    func facebookAppInvite() {
        let content = FBSDKAppInviteContent()
        content.appLinkURL = URL.init(string: "https://fb.me/472765793056655")!
        content.appInvitePreviewImageURL = URL.init(string: "https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/17990919_1384093181683283_2629335071562433522_n.jpg?oh=039c6b8944fc19a594994f3f812ec4b7&oe=598114D2")!
        FBSDKAppInviteDialog.show(from: self, with: content, delegate: self)
    }
    func inviteFinished(withInvitations invitationIds: [String], error: Error?) {
        if let error = error {
            print("Failed: " + error.localizedDescription)
        } else {
            print("\(invitationIds.count) invites sent")
        }
    }
    
    public func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
    }
    
    public func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        
    }
    
}
extension FLOMenuViewController : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if error != nil {
            
            return
        }
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.firebaseInviteAppViaGoogle()
        }
        
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
}
