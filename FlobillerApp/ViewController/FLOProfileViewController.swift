//
//  FLOProfileViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/2/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import SDWebImage

class FLOProfileViewController: FunctionBaseViewController, UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!

    var countriesArray : [CountryModel]?
    var base_load_image_url : String?
    var accountInformation : AccountInformation?
    
    let datePicker = DatePickerView.initWithNib()
    let countryPicker = CustomPickerView.initWithNib()
    var loadedApi = false

    var indexCountry = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        header?.buttonSave.isHidden = false
        countriesArray = CommonUltilities.getListCountry()
        

        self.setUpDatePicker()
        self.setUpTFDelegate()
        self.setUpCountryPicker()
        self.rightButtonBarAction = {
            self.view.endEditing(true)
            self.upDateUserAccount()
            self.datePicker?.isHidden = true
            self.countryPicker?.isHidden = true
        }




    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !loadedApi {
            self.getUserInformation()
        }


    }
    func getUserInformation() {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getUserInfo(success: { accountInfo in
            self.accountInformation = accountInfo
            self.firstNameTextField.text = accountInfo?.firstName
            self.lastNameTextField.text = accountInfo?.lastName
            self.dobTextField.text = accountInfo?.dateOfBirth
            self.mobileTextField.text = accountInfo?.mobile

            self.emailTextField.text = accountInfo?.email
            for i in 0...self.countriesArray!.count - 1 {

                if self.countriesArray![i].key == accountInfo?.country {
                    self.indexCountry = i
                    self.countryPicker?.picker.selectRow(i, inComponent: 0, animated: false)
                    self.countryTextField.text = self.countriesArray![i].name
                }
            }

            APIConnect.getCountriesUser({ (baseURL, extesion)  in
                self.base_load_image_url = baseURL
                let url = URL(string: baseURL! + (accountInfo?.country)! + "." + extesion!)
                self.countryImageView.sd_setImage(with: url)

            })
            self.view.hideActivityView()
        }, {
            self.view.hideActivityView()
        })

    }
    func upDateUserAccount() {
        let tmpInfo = AccountInformation()
        tmpInfo.country = countriesArray?[indexCountry].key
        tmpInfo.lastName = lastNameTextField.text
        tmpInfo.firstName = firstNameTextField.text
        tmpInfo.dateOfBirth = dobTextField.text
        tmpInfo.mobile = mobileTextField.text

        if tmpInfo.lastName == BLANK || tmpInfo.dateOfBirth == BLANK || tmpInfo.firstName == BLANK || tmpInfo.mobile == BLANK {
            let toast = MDToast.init(text: "All field must be filled", duration: 1)
            toast.show()
            return
        }
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.updateUserInfor(tmpInfo, success: {

            self.view.hideActivityView()
            let toast = MDToast.init(text: "Update Successfully", duration: 1)
            toast.show()
        }) { 
            self.view.hideActivityView()
            let toast = MDToast.init(text: "Update failed", duration: 1)
            toast.show()

        }
    }
    @IBAction func openCountryPicker(_ sender: Any) {
        self.view.endEditing(true)
        countryPicker?.isHidden = false
        datePicker?.isHidden = true
        self.countryPicker?.picker.selectRow(indexCountry, inComponent: 0, animated: false)
    }
    @IBAction func openDatepicker(_ sender: Any) {
        self.view.endEditing(true)
        datePicker?.isHidden = false
        countryPicker?.isHidden = true
        if let dateSelect = self.dobTextField.text?.toNSDate() {
            datePicker?.picker.date = dateSelect
        }

    }
    func setUpTFDelegate() {
        self.firstNameTextField.delegate = self
        self.lastNameTextField.delegate = self
        self.mobileTextField.delegate = self
        self.countryTextField.delegate = self

    }
    func setUpDatePicker() {
        datePicker?.frame = CGRect.init(x: 0, y: self.view.frame.size.height - 244, width: self.view.frame.size.width, height: 244)
        self.view.addSubview(datePicker!)
        datePicker?.isHidden = true
        datePicker?.doneBlock = {
            self.datePicker?.isHidden = true
            self.dobTextField.text = self.datePicker?.picker.date.toNSString()
        }
        datePicker?.cancelBlock = {
            self.datePicker?.isHidden = true

        }

    }

    func setUpCountryPicker() {
        countryPicker?.frame = CGRect.init(x: 0, y: self.view.frame.size.height - 244, width: self.view.frame.size.width, height: 244)
        self.view.addSubview(countryPicker!)
        countryPicker?.isHidden = true
        countryPicker?.titlePicker.text = "Country"
        countryPicker?.doneClosure = {
            self.countryPicker?.isHidden = true
            self.indexCountry = self.countryPicker!.picker.selectedRow(inComponent: 0)
            self.countryTextField.text = self.countriesArray![self.indexCountry].name
            let url = URL(string: self.base_load_image_url! + self.countriesArray![self.indexCountry].key! + ".png" )


            self.countryImageView.sd_setImage(with: url)
        }
        countryPicker?.cancelClosure = {
            self.countryPicker?.isHidden = true

        }
        countryPicker?.picker.delegate = self
        countryPicker?.picker.dataSource = self

    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.datePicker?.isHidden =  true
        self.countryPicker?.isHidden = true
    }


    // MARK: - Picker view datasoure, picker view delegate

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countriesArray?[row].name
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countriesArray!.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

    }



}
extension String {
    func toNSDate() -> Date? {
        let dateFormatter = DateFormatter()
        // this is imporant - we set our input date format to match our input string
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: self)
    }
}
extension Date {
    func toNSString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"

        return formatter.string(from: self )
    }
}
