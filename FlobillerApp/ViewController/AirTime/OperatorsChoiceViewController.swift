//
//  OperatorsChoiceViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/21/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class OperatorsChoiceViewController: FunctionBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var operators : [OperatorModel]?
    var userInfo : AccountInformation?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.getApiData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getApiData() {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getUserInfo(success: { accountInfo in
            self.userInfo = accountInfo
            APIConnect.getAllOperators(accountInfo?.country ?? BLANK, success: { arrayResult in
                self.operators = arrayResult as? [OperatorModel]
                self.tableView.reloadData()
                self.hideErrorView()
                self.view.hideActivityView()
            }) {
                self.showErrorView("There are no Operators available for your selected country", tryLaterClosure: {
                    self.getApiData()
                })
                self.view.hideActivityView()
            }
        }) {
            self.view.hideActivityView()
        }

    }
}
extension OperatorsChoiceViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return operators?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OperatorCell") as? OperatorCell
        if let url = URL.init(string: operators![indexPath.row].logo!) {
            cell?.countryImageView.sd_setImage(with: url)
        }

        cell?.nameLabel.text = operators![indexPath.row].name
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
                let storyboard = UIStoryboard.init(name: "AirTime", bundle: nil)
                let subVC = storyboard.instantiateViewController(withIdentifier: "operatorRecharge") as? OperatorRechargeViewController
                subVC?.operatorObject = operators![indexPath.row]
                subVC?.userInfor = userInfo
                subVC?.naviTitle = "AIRTIME"
                self.navigationController?.pushViewController(subVC!, animated: true)
    }
}

class OperatorCell: UITableViewCell {

    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
}
