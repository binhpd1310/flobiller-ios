//
//  OperatorRechargeViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/21/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit



class OperatorRechargeViewController: FunctionBaseViewController, UITextFieldDelegate {

    var userInfor : AccountInformation?
    var operatorObject : OperatorModel?
    var productArray : [ProductOperatorModel]?

    var productButtonArray : [UIButton]?

    @IBOutlet weak var tpScrollView: UIScrollView!
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameOperator: UILabel!

    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var phoneCodeTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    var productSelected = -1

    @IBOutlet weak var buttonRecharge: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        productButtonArray = [UIButton]()
        buttonRecharge.layer.cornerRadius = 4
        viewBorder.layer.cornerRadius = 6
        viewBorder.layer.borderWidth = 2
        viewBorder.layer.borderColor = MAIN_COLOR.cgColor

        nameOperator.text = operatorObject!.name
        if let url = URL.init(string: operatorObject!.logo ?? BLANK) {
            imageView.sd_setImage(with: url)
        }
        phoneCodeTF.text = userInfor?.countryCode
        phoneNumberTF.text = userInfor?.mobile

        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")

        APIConnect.getAllAirtimeTopups(operatorObject!.operatorId ?? BLANK, success: { arrayResult in
            self.productArray =  arrayResult as? [ProductOperatorModel]
            self.createButtonProduct(arrayResult as! [ProductOperatorModel])
            self.view.hideActivityView()
        }) {
            self.view.hideActivityView()
        }

    }

    func createButtonProduct(_ productArray : [ProductOperatorModel]) {
        if productArray.count == 0 {
            return
        }
        scrollView.contentSize = CGSize.init(width: 80.0 * CGFloat(productArray.count), height: scrollView.frame.size.height)
        for i in 0...productArray.count - 1 {
            let button = UIButton.init(frame: CGRect.init(x: (80.0 * Double(i)) + 20, y: 0.0 , width: 60.0, height: 34))
            button.layer.borderWidth = 1.5
            button.layer.borderColor = UIColor.black.cgColor
            button.setButtonProductTitle(productArray[i].name ?? BLANK, currency: productArray[i].currency ?? BLANK)
            button.tag = i
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapButton(_:)))
            button.addGestureRecognizer(tapGesture)
            button.layer.cornerRadius = 4
            button.titleLabel?.font = UIFont.systemFont(ofSize: 13);
            productButtonArray?.append(button)
            scrollView.addSubview(button)
        }
    }
    func tapButton(_ sender: UITapGestureRecognizer) {
        let button = sender.view! as? UIButton
        button?.backgroundColor = UIColor.init(red: 96.0/255, green: 200.0/255, blue: 242.0/255, alpha: 1)


        productSelected = button!.tag
        for btn in productButtonArray! {
            if btn != button {
                btn.backgroundColor = UIColor.white
            }
        }
        
    }
    @IBAction func buttonRechargeNowAction(_ sender: Any) {
        if phoneCodeTF.text == BLANK || phoneNumberTF.text == BLANK {
            CommonUltilities.showMessage("All fields are mandatory", isError: true, actionBlock: { 
                KGModal.sharedInstance().hide(animated: true)
            })
            return
        }
        if productSelected == -1 {
            CommonUltilities.showMessage("Fields are required", isError: true, actionBlock: {
                KGModal.sharedInstance().hide(animated: true)
            })
            return
        }
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        let invoice = createInvoiceInput()
        APIConnect.createInvoice(invoice, success: { invoiceID in
            self.view.hideActivityView()
            let storyboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
            let confirmVC = storyboard.instantiateViewController(withIdentifier: "payBillConfirm") as? PayBillConfirmViewController
//            confirmVC.nait

            guard let account = self.userInfor else {
                return
            }
            confirmVC?.accountNumber = (account.countryCode ?? BLANK) + (account.mobile ?? BLANK )
            confirmVC?.invoiceNo = invoiceID
            confirmVC?.accountName = (account.firstName ?? BLANK) + " " + (account.lastName ?? BLANK)
            confirmVC?.amount = invoice.amount
            confirmVC?.subcriptionName = invoice.name
            confirmVC?.subcriptionId = invoiceID
            confirmVC?.currency = invoice.currency
            confirmVC?.country = account.country
            confirmVC?.email = account.email
            confirmVC?.phoneCode = account.countryCode
            confirmVC?.phoneNumber = account.mobile
            confirmVC?.firstName = account.firstName
            confirmVC?.lastName = account.lastName
            self.navigationController?.pushViewController(confirmVC!, animated: true)
        }) { msgError in
            self.view.hideActivityView()
            if let msg = msgError {
                CommonUltilities.showMessage(msg, isError: true, actionBlock: {
                    KGModal.sharedInstance().hide(animated: true)
                })
            }
        }

    }

    func createInvoiceInput() -> InvoiceInput {
        let invoice = InvoiceInput()
        invoice.api = "airtime"
        invoice.auth_token = AppDelegate.shareIntance().authenToken!
        invoice.operator_id  = operatorObject?.operatorId
        invoice.currency = productArray![productSelected].currency
        invoice.amount = productArray![productSelected].retailPrice
        invoice.countrycode = phoneCodeTF.text
        invoice.mobile = phoneNumberTF.text
        invoice.name = operatorObject?.name
        invoice.cust_topup = "\(invoice.currency!) \(productArray![productSelected].name!)"
        return invoice
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        tpScrollView.contentOffset = CGPoint.init(x: 0, y: -20)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension UIButton {
    func setButtonProductTitle(_ product: String, currency : String) {
        let att = NSMutableAttributedString(string: "\(currency)\(product)");
        att.addAttribute(NSForegroundColorAttributeName, value: MAIN_COLOR, range: NSRange(location: currency.characters.count, length: product.characters.count))
        att.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: currency.characters.count))
        self.setAttributedTitle(att, for: .normal)
    }
}
