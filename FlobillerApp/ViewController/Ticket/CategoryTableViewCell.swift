//
//  CategoryTableViewCell.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var imageCategory: UIImageView!

    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    
    }

}
