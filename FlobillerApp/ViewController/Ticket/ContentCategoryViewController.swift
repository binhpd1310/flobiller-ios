//
//  ContentCategoryViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class ContentCategoryViewController: FunctionBaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var typeEvent : String?

    var contentArray = [ContentCategoryModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getUserInfo(success: { accountInfo  in
            APIConnect.getContentCategories(self.typeEvent ?? BLANK, country: accountInfo?.country ?? BLANK, successClosure: { result in
                self.contentArray = result
                self.tableView.reloadData()
                self.view.hideActivityView()
            }, failure: { 
                self.view.hideActivityView()
            })
            
        }) {
            self.view.hideActivityView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}

extension ContentCategoryViewController : UITableViewDelegate,UITableViewDataSource {
    func  tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "categoryDetailCell") as? DetailCategoryView
        if cell == nil {
           cell = (Bundle.main.loadNibNamed("DetailCategoryView", owner: self, options: nil)![0] as! DetailCategoryView)
        }
        let contentModel = contentArray[indexPath.row]
        if let url = URL.init(string: contentModel.poster ?? BLANK) {
            cell?.bgImageView.sd_setImage(with: url)
        } else {
            cell?.bgImageView = nil
        }
        cell?.nameLabel.text = contentModel.name
        cell?.cityLabel.text = contentModel.city
        cell?.addressLabel.text = contentModel.address
        cell?.amountLabel.text  = "From \(contentModel.amount ?? BLANK) \(contentModel.currency ?? BLANK)"

        return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  contentArray.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ticketStoryboard = UIStoryboard.init(name: "Ticket", bundle: nil)
        let rootVC = ticketStoryboard.instantiateViewController(withIdentifier: "detailTicketVC") as? TicketDetailViewController
        rootVC?.naviTitle = contentArray[indexPath.row].name ?? BLANK
        rootVC?.contentDetail = contentArray[indexPath.row]
        rootVC?.needDismiss = false
        _ = self.navigationController?.pushViewController(rootVC!, animated: true)

    }
}
