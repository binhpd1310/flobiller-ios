//
//  TicketCategoryViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class TicketCategoryViewController: FunctionBaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var categories : [TicketCategory]?
    override func viewDidLoad() {
        super.viewDidLoad()

        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getTicketCatergories({ result in
            self.categories =  result
            self.tableView.reloadData()
            self.view.hideActivityView()
        }) { 
            self.view.hideActivityView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
}

extension TicketCategoryViewController : UITableViewDataSource,UITableViewDelegate {
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell") as? CategoryTableViewCell
        cell?.nameLabel.text = self.categories?[indexPath.row].name ?? BLANK
        if let url = URL.init(string: self.categories?[indexPath.row].logo ?? BLANK) {
            cell?.imageCategory.sd_setImage(with: url)
        } else {
            cell?.imageCategory.image = nil
        }

        return cell!
    }

    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ticketStoryboard = UIStoryboard.init(name: "Ticket", bundle: nil)
        let rootVC = ticketStoryboard.instantiateViewController(withIdentifier: "contentCategory") as? ContentCategoryViewController
        rootVC?.naviTitle = self.categories?[indexPath.row].name ?? BLANK
        rootVC?.typeEvent = self.categories?[indexPath.row].type
        rootVC?.needDismiss = false
        _ = self.navigationController?.pushViewController(rootVC!, animated: true)
    }



}
