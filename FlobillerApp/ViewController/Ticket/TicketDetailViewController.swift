//
//  TicketDetailViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/28/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class TicketDetailViewController: FunctionBaseViewController {

    @IBOutlet weak var viewTop: UIView!
    var contentDetail : ContentCategoryModel?
    @IBOutlet weak var label: UILabel!
    var groupEventByDateAndTime = [[EventTicket]]()

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomSpaceConstraint: NSLayoutConstraint!

    @IBOutlet weak var noTicketLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var viewOpenMap: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var buttonBookNow: UIButton!

    var bookTicketView : BookTicketView?

    override func viewDidLoad() {
        super.viewDidLoad()
        let htmlString = "<html><head><title></title></head><body style=\"background:transparent;\">" + (contentDetail!.short_desc ?? BLANK) + (contentDetail!.desc ?? BLANK) + "</body></html>"
        do {
            let str = try NSAttributedString(data: htmlString.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)

            label.attributedText = str
        } catch {
            print(error)
        }
        setupTopView()
        cityLabel.text = contentDetail?.city ?? BLANK
        addressLabel.text = contentDetail?.address ?? BLANK

        viewAddress.layer.cornerRadius = 3
        noTicketLabel.isHidden = true
        buttonBookNow.layer.cornerRadius = 3

        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getEventId(contentDetail?.eventId ?? BLANK, successClosure: { eventTicketArray in
            self.view.hideActivityView()
            self.groupEventByDateAndTime = self.groupTicketByProperty("ticket_day", fromArray: eventTicketArray)
            if eventTicketArray.count == 0 {
                self.noTicketLabel.isHidden = false
                self.buttonBookNow.isEnabled = false
                self.buttonBookNow.setTitleColor(UIColor.lightGray, for: .normal)
            } else {

                self.setUpViewChooseTicket()
            }
        }) { 
            self.view.hideActivityView()
        }

        // Do any additional setup after loading the view.
    }
    func setUpViewChooseTicket() {
        bookTicketView = Bundle.main.loadNibNamed("BookTicketView", owner: self, options: nil)?[0] as? BookTicketView
        bookTicketView?.frame =  CGRect.init(x: 0, y: viewOpenMap.frame.size.height + viewOpenMap.frame.origin.y + 2, width: self.view.frame.width, height: 420)
        bookTicketView?.ticketGroup = self.groupEventByDateAndTime
        bookTicketView?.displayView()
        bottomSpaceConstraint.constant = 430
        scrollView.addSubview(bookTicketView!)
    }

    func setupTopView() {
        let cell = Bundle.main.loadNibNamed("DetailCategoryView", owner: self, options: nil)![0] as! DetailCategoryView


        if let url = URL.init(string: contentDetail?.poster ?? BLANK) {
            cell.bgImageView.sd_setImage(with: url)
        } else {
            cell.bgImageView = nil
        }
        cell.nameLabel.text = contentDetail?.name
        cell.cityLabel.text = contentDetail?.city
        cell.addressLabel.text = contentDetail?.address
        cell.amountLabel.text  = "$"
        DispatchQueue.main.async {
            cell.frame = self.viewTop.bounds
            self.viewTop.addSubview(cell)
        }


    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func openMapButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Ticket", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "mapViewVC") as! MapViewController
        guard let lat = Double(contentDetail?.map_lat ?? "0"), let longMap = Double(contentDetail?.map_long ?? "0") else {
            return
        }
        vc.latitude = lat
        vc.longitude = longMap
        vc.naviTitle = "MAP"
        _ =  self.navigationController?.pushViewController(vc, animated: true)

    }

    @IBAction func bookNowButtonAction(_ sender: Any) {
        guard let eventId = contentDetail?.eventId, let ticket = bookTicketView?.selectedTicket, let content = contentDetail else {
            return
        }

        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.eventQuantity(eventId, successClosure: { 
            APIConnect.getUserInfo(success: { account in
                APIConnect.eventAdd(ticket, contentDetail: content, userInfo: account!, quantity: self.bookTicketView?.numberTicker.text ?? BLANK, amount: self.bookTicketView?.totalLabel.text ?? BLANK, successClosure: { invoiceNo in
                    self.view.hideActivityView()
                    let storyboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
                    let confirmVC = storyboard.instantiateViewController(withIdentifier: "payBillConfirm") as? PayBillConfirmViewController
                    confirmVC?.accountNumber = (account?.countryCode ?? BLANK) + (account?.mobile ?? BLANK )
                    confirmVC?.invoiceNo = invoiceNo
                    confirmVC?.accountName = (account?.firstName ?? BLANK) + " " + (account?.lastName ?? BLANK)
                    confirmVC?.amount = self.bookTicketView?.totalLabel.text
                    confirmVC?.subcriptionName = content.name ?? BLANK
                    confirmVC?.subcriptionId = invoiceNo
                    confirmVC?.currency = ticket.currency ?? BLANK
                    confirmVC?.country = account?.country
                    confirmVC?.email = account?.email
                    confirmVC?.phoneCode = account?.countryCode
                    confirmVC?.phoneNumber = account?.mobile
                    confirmVC?.firstName = account?.firstName
                    confirmVC?.lastName = account?.lastName
                    self.navigationController?.pushViewController(confirmVC!, animated: true)

                }, failure: { 
                    self.view.hideActivityView()
                    CommonUltilities.showMessage("Connection error, please check connection and try again!", isError: true, actionBlock: { 
                        KGModal.sharedInstance().hide(animated: true)
                    })

                })
            }, {

                self.view.hideActivityView()
                CommonUltilities.showMessage("Connection error, please check connection and try again!", isError: true, actionBlock: {
                    KGModal.sharedInstance().hide(animated: true)
                })
            })
        }) { 
            self.view.hideActivityView()
            CommonUltilities.showMessage("Connection error, please check connection and try again!", isError: true, actionBlock: {
                KGModal.sharedInstance().hide(animated: true)
            })
        }
    }
    func groupTicketByProperty(_ property : String, fromArray : [EventTicket]) -> [[EventTicket]]{
        var arrString = [String]()
        for event in fromArray {

            if !arrString.contains(event.value(forKey: property) as! String) {
                arrString.append(event.value(forKey: property) as! String)
            }
        }
        var result = [[EventTicket]]()
        for str in arrString {
            let arrayFilter = fromArray.filter {
                $0.value(forKey: property) as! String == str
            }
            result.append(arrayFilter)

        }
        return result
    }
    

}
