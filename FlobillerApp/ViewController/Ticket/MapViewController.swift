//
//  MapViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 4/11/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: FunctionBaseViewController {
    var latitude : Double =  0
    var longitude : Double = 0
    @IBOutlet weak var mkMapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        

        let mapRegion = MKCoordinateRegion.init(center: CLLocationCoordinate2D.init(latitude: latitude, longitude: longitude), span: MKCoordinateSpan.init(latitudeDelta: 0.2, longitudeDelta: 0.2))
        mkMapView.setRegion(mapRegion, animated: true)
        let point = MKPointAnnotation.init()
        point.coordinate = CLLocationCoordinate2D.init(latitude: latitude, longitude: longitude)
        mkMapView.addAnnotation(point)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
