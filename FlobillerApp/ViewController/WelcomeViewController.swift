//
//  WelcomeViewController.swift
//  FlobillerApp
//
//  Created by Monkeyyyy on 11/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    var image : UIImage?

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: "FLOdidShowWelcome")
        imageView.image = image
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func skipButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

}
