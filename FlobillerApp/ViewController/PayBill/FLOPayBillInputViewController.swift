//
//  FLOPayBillInputViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/14/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit


class FLOPayBillInputViewController: FunctionBaseViewController {

    var billerObject : BillerModel?

    var subcriptionObject : SubcriptionModel?
    var isAutoShow : Bool?
    var userInfo : AccountInformation?

    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var nameHeaderLabel: UILabel!

    @IBOutlet weak var accountNameTF: UITextField!
    @IBOutlet weak var accountNumberTF: UITextField!
    @IBOutlet weak var invoiceNumberTF: UITextField!
    @IBOutlet weak var amountTF: UITextField!
    
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var payBillButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        payBillButton.layer.cornerRadius = 4.0

        currencyLabel.text = userInfo?.currency
        APIConnect.getCountriesUser({ (baseURL, extesion)  in
            let url = URL(string: baseURL! + (self.billerObject?.country)! + "." + extesion!)
            self.countryImageView.sd_setImage(with: url)
        })
        let countriesArray = CommonUltilities.getListCountry()
        for country in countriesArray {
            if country.key == billerObject?.country {
                countryLabel.text = country.name
                break
            }
        }
        if let url = URL.init(string: URL_LOGO_PAYBILLER + billerObject!.logo!) {
            logoImageView.sd_setImage(with: url)
        }
        nameLabel.text = billerObject!.name
        nameHeaderLabel.text = billerObject!.name
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func payBillButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        if accountNameTF.text == BLANK || accountNumberTF.text == BLANK || amountTF.text == BLANK {
            CommonUltilities.showMessage("You must fill all fields", isError: true, actionBlock: { 
               KGModal.sharedInstance().hide(animated: true)
            })
            return
        }

        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        let input = createInvoiceInput()

        APIConnect.createInvoice(input, success: { invoiceID in
            self.view.hideActivityView()
            let storyboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
            let confirmVC = storyboard.instantiateViewController(withIdentifier: "payBillConfirm") as? PayBillConfirmViewController

            confirmVC?.invoiceNo = invoiceID
            confirmVC?.accountNumber = self.accountNumberTF.text
            confirmVC?.accountName = self.accountNameTF.text
            confirmVC?.amount = self.amountTF.text 
            confirmVC?.subcriptionName = self.subcriptionObject?.name
            confirmVC?.subcriptionId = self.subcriptionObject?.id
            confirmVC?.currency = self.userInfo?.currency
            confirmVC?.country = self.userInfo?.country
            confirmVC?.email = self.userInfo?.email
            confirmVC?.phoneCode = self.userInfo?.countryCode
            confirmVC?.phoneNumber = self.userInfo?.mobile
            confirmVC?.firstName = self.userInfo?.firstName
            confirmVC?.lastName = self.userInfo?.lastName

            self.navigationController?.pushViewController(confirmVC!, animated: true)
        }) { msgError in
            self.view.hideActivityView()
            if let msg = msgError {
                CommonUltilities.showMessage(msg, isError: true, actionBlock: { 
                    KGModal.sharedInstance().hide(animated: true)
                })
            }
        }
    }
    func createInvoiceInput() -> InvoiceInput {
        let invoice = InvoiceInput()
        invoice.api = "paybill"
        invoice.auth_token = AppDelegate.shareIntance().authenToken!
        invoice.billerid  = billerObject?.billerId
        invoice.billerinvoice = subcriptionObject?.id
        invoice.currency = userInfo?.currency ?? BLANK
        invoice.amount = amountTF.text
        invoice.country = billerObject?.country
        invoice.cust_account_name = accountNameTF.text
        invoice.cust_account_number = accountNumberTF.text
        invoice.name = billerObject?.name
        invoice.cust_topup = invoiceNumberTF.text
        return invoice
    }




}
