//
//  PayBillConfirmViewController.h
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/17/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PayBillConfirmViewController : UIViewController
@property (copy, nonatomic) NSString *navigationBarTitle;
@property (copy, nonatomic) NSString *invoiceNo;
@property (copy, nonatomic) NSString *accountName;
@property (copy, nonatomic) NSString *accountNumber;
@property (copy, nonatomic) NSString *amount;
@property (copy, nonatomic) NSString *subcriptionName;
@property (copy, nonatomic) NSString *subcriptionId;

//======================
@property (copy, nonatomic) NSString *currency;
@property (copy, nonatomic) NSString *country;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *phoneCode;
@property (copy, nonatomic) NSString *phoneNumber;
@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
//======================

@property (weak, nonatomic) IBOutlet UILabel *billerLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIButton *payNowButton;
@property (weak, nonatomic) IBOutlet UILabel *payStatusLabel;

@end
