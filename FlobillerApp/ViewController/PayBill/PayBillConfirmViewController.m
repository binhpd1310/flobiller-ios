//
//  PayBillConfirmViewController.m
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/17/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

#import "PayBillConfirmViewController.h"
#import "FlobillerApp-Swift.h"
#import <FlobillerFramework/FlocashService.h>
#import "CheckingNetwork.h"
#import "MDToast.h"
#import "UIView+RNActivityView.h"

@interface PayBillConfirmViewController () {
    BOOL reloadData;
}

@end

@implementation PayBillConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addHeaderView];
    self.invoiceNoLabel.text = self.invoiceNo;
    self.nameLabel.text = self.accountName;
    self.accountNoLabel.text = self.accountNumber;
    self.billerLabel.text = self.subcriptionName;
    self.amountLabel.text = [NSString stringWithFormat:@"%@ %@",_currency,_amount ? _amount : @""];


    _payNowButton.layer.cornerRadius = 6;
    reloadData = NO;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (reloadData) {

        if (![CheckingNetwork shareIntance].networkStatus) {
            [[[MDToast alloc] initWithText:@"No Internet connection" duration:1] show];
            return;
        }
        [self.view showActivityViewWithLabel:@"Loading"];
        [APIConnect getInvoicesByOrderId:self.invoiceNo successClosure:^(InvoiceModel *invoice) {
            [self.view hideActivityView ];
            self.nameLabel.text = invoice.cust_account_name;
            self.accountNoLabel.text = invoice.cust_account_number;
            self.billerLabel.text = invoice.name;
            self.amountLabel.text = [NSString stringWithFormat:@"%@ %@",_currency,_amount ? _amount : @""];
            if ([invoice.flocash_status isEqualToString:@"S" ]) {
                self.payStatusLabel.text = @"Paid";
            } else if ([invoice.flocash_status isEqualToString:@"P"]) {
                self.payStatusLabel.text = @"Pending";
            } else {
                self.payStatusLabel.text = @"Not paid";
            }
        } failure:^{
            [self.view hideActivityView];
       }];
    }
    reloadData = YES;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)payNowButtonAction:(id)sender {
    [FlocashService sharedInstance].evironment = SANDBOX;
    Request *request = [[Request alloc] init];

    OrderInfo *order = [[OrderInfo alloc] init];
    order.amount = [_amount doubleValue];
    order.currency = self.currency;
    order.item_name =  self.subcriptionName;
    order.item_price = _amount;
    order.orderId = self.invoiceNo;
    order.quantity = @"1";

    PayerInfo *payer = [[PayerInfo alloc] init];

    payer.country = self.country;
    payer.firstName = self.firstName;
    payer.lastName = self.lastName;
    payer.email = self.email;
    payer.phoneCode = self.phoneCode;
    payer.phoneNumber = self.phoneNumber;
    payer.mobile = [NSString stringWithFormat:@"%@%@",payer.phoneCode,payer.phoneNumber];

    MerchantInfo *merchant = [[MerchantInfo alloc] init];

    request.order = order;
    request.payer = payer;
    request.merchant = merchant;
    merchant.merchantAccount = @"flobiller@flocash.com";
    
    [FlocashService sharedInstance].evironment = SANDBOX;
    [[FlocashService sharedInstance] createOrder:request presentCreateOrderViewFrom:self];

}
- (void)addHeaderView {
    NavigationPrestedView *headerView = [NavigationPrestedView initWithTitle:@"PAYMENT"];
    headerView.buttonSave.hidden = YES;
    headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 64)  ;
    headerView.buttonBackClousure = ^() {
        [self dismissViewControllerAnimated:YES completion:nil];
    };
    [self.view addSubview:headerView];
}





@end
