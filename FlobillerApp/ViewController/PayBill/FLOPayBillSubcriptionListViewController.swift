//
//  FLOPayBillSubcriptionListViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/16/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class SubcriptionModel : NSObject {
    var name : String?
    var id : String?
}

class FLOPayBillSubcriptionListViewController: FunctionBaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var billerObject : BillerModel?
    var userInfo : AccountInformation?
    var subcriptionArray : [SubcriptionModel]?
    override func viewDidLoad() {
        super.viewDidLoad()
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getListSubcriptions(billerObject!, success: { array in
            self.subcriptionArray = array as? [SubcriptionModel]
            if self.subcriptionArray?.count == 1 {
                self.showInputViewWithSubcription(self.subcriptionArray!.first!,isAutoShow: true)
            } else {
                self.tableView.reloadData()
            }

            self.view.hideActivityView()
        }) { 
            self.view.hideActivityView()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    



}
extension FLOPayBillSubcriptionListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return subcriptionArray?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "billerCellReuse") as? BillerCell

        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        showInputViewWithSubcription(subcriptionArray![indexPath.row],isAutoShow: false)
    }

    func showInputViewWithSubcription(_ sub : SubcriptionModel, isAutoShow : Bool) {
        let storyboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
        let subVC = storyboard.instantiateViewController(withIdentifier: "payBillForm") as? FLOPayBillInputViewController
        subVC?.billerObject = billerObject
        subVC?.userInfo = userInfo
        subVC?.subcriptionObject = sub
        subVC?.naviTitle = "PAY BILL"
        subVC?.isAutoShow = isAutoShow
        self.navigationController?.pushViewController(subVC!, animated: true)
    }
}
