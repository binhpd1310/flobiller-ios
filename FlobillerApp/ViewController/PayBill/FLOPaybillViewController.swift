//
//  FLOPaybillViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/13/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class FLOPaybillViewController: FunctionBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var billerArray : NSArray?
    var userInfo : AccountInformation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getUserInfo(success: { userInfo in
            self.userInfo = userInfo
            APIConnect.getAllBillers(success: { array in
                self.view.hideActivityView()
                self.billerArray = array
                self.tableView.reloadData()
            }) {
                self.view.hideActivityView()
            }
        }) { 
            self.view.hideActivityView()
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}

extension FLOPaybillViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return billerArray?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "billerCellReuse") as? BillerCell
        if let url = URL.init(string: URL_LOGO_PAYBILLER + (billerArray?[indexPath.row] as! BillerModel).logo!) {
            cell?.billerImage.sd_setImage(with: url)
        }

        cell?.nameLabel.text = (billerArray?[indexPath.row] as! BillerModel).name
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let storyboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
        let subVC = storyboard.instantiateViewController(withIdentifier: "subcriptionList") as? FLOPayBillSubcriptionListViewController
        subVC?.billerObject = billerArray?[indexPath.row] as? BillerModel
        subVC?.userInfo = userInfo
        subVC?.naviTitle = "PAY BILL"
        self.navigationController?.pushViewController(subVC!, animated: true)

    }


}

class BillerCell : UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var billerImage: UIImageView!
    override func awakeFromNib() {

    }
}
