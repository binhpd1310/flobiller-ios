//
//  GiftCardTableViewCell.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/9/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class GiftCardTableViewCell: UITableViewCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageGiftView: UIImageView!
    @IBOutlet weak var nameGiftCardLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        borderView.layer.borderWidth = 1.5
        borderView.layer.borderColor = MAIN_COLOR.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
