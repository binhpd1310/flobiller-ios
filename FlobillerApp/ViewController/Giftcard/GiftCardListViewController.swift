//
//  GiftCardListViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/9/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class GiftCardListViewController: FunctionBaseViewController {


    @IBOutlet weak var tableView: UITableView!
    var giftCardArr = [GiftCardModel]()
    var userInfor : AccountInformation?
    override func viewDidLoad() {
        super.viewDidLoad()
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: "Loading")
        APIConnect.getAllGiftCard({ arrayResult in
            self.giftCardArr = arrayResult
            self.tableView.reloadData()
            self.view.hideActivityView()
        }) { 
            self.view.hideActivityView()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension GiftCardListViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "GiftCardCell") as! GiftCardTableViewCell
        if let url = URL.init(string: giftCardArr[indexPath.row].logo) {
            cell.imageGiftView.sd_setImage(with: url)
        }
        cell.nameGiftCardLabel.text = giftCardArr[indexPath.row].name
        var widthButton : CGFloat
        let numberButton = giftCardArr[indexPath.row].amountArr.count
        if numberButton >= 4 {
            widthButton  = (cell.scrollView.frame.size.width - 60) / CGFloat(4)
            cell.scrollView.contentSize = CGSize.init(width: (widthButton + 20) * CGFloat(numberButton), height: 38)
        } else {
            widthButton =   (cell.scrollView.frame.size.width - CGFloat(20) * CGFloat(numberButton - 1)) / CGFloat(numberButton)
        }

        for view in cell.scrollView.subviews {
            view.removeFromSuperview()
        }
        for i in 0..<giftCardArr[indexPath.row].amountArr.count {
            let orginX = (widthButton + 20) * CGFloat(i)
            let button = UIButton.init(frame: CGRect.init(x: orginX, y: 0, width: widthButton, height: 32))
            button.tag = 1000 * indexPath.row + i
            button.setTitle(giftCardArr[indexPath.row].amountArr[i], for: .normal)
            button.setTitleColor(.black, for: .normal)
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.darkGray.cgColor
            button.addTarget(self, action: #selector(buttonGiftCardAction(button:)), for: .touchUpInside)
            cell.scrollView.addSubview(button)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return giftCardArr.count
    }
    func buttonGiftCardAction(button: UIButton) {
        let rowPress = button.tag /  1000
        let indexPress = button.tag % 1000
        let input = InvoiceInput()
        input.amount = giftCardArr[rowPress].amountArr[indexPress]
        input.giftcardid = giftCardArr[rowPress].id
        input.giftcardname = giftCardArr[rowPress].name
        input.api = "giftcard"
        input.auth_token = AppDelegate.shareIntance().authenToken
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 1)
            toast.show()
            return
        }
        self.view.showActivityView(withLabel: BLANK)
        APIConnect.createInvoice(input, success: { invoiceID in
            CommonUltilities.showMessage("Invoice added Successfully.", leftButtonTitle: "CANCEL", rightButtonTitle: "PAY NOW", isError: false, actionBlock: { 
                KGModal.sharedInstance().hide()
            }, rightActionBlock: {
                KGModal.sharedInstance().hide()
                if self.userInfor == nil {
                    APIConnect.getUserInfo(success: { accountInfo  in
                        self.userInfor = accountInfo
                        self.goToConfirmScreen(invoiceID!, name: input.giftcardname!, amount: input.amount!)
                    }) {
                        
                    }

                } else {
                    self.goToConfirmScreen(invoiceID!, name: input.giftcardname!, amount: input.amount!)
                }

            })
            self.view.hideActivityView()
        }) { msgError in
            self.view.hideActivityView()
            if let msg = msgError {
                self.view.hideActivityView()
                CommonUltilities.showMessage(msg, isError: true, actionBlock: {
                    KGModal.sharedInstance().hide(animated: true)
                })
            }
        }
    }
    func goToConfirmScreen(_ invoiceId : String, name : String, amount : String) {
        let account = self.userInfor
        let storyboard = UIStoryboard.init(name: "PaybillStoryboard", bundle: nil)
        let confirmVC = storyboard.instantiateViewController(withIdentifier: "payBillConfirm") as? PayBillConfirmViewController
        confirmVC?.accountNumber = (account?.countryCode ?? BLANK) + (account?.mobile ?? BLANK )
        confirmVC?.invoiceNo = invoiceId
        confirmVC?.accountName = (account?.firstName ?? BLANK) + " " + (account?.lastName ?? BLANK)
        confirmVC?.amount = amount
        confirmVC?.subcriptionName = name
        confirmVC?.subcriptionId = invoiceId
        confirmVC?.currency = "USD"
        confirmVC?.country = account?.country
        confirmVC?.email = account?.email
        confirmVC?.phoneCode = account?.countryCode
        confirmVC?.phoneNumber = account?.mobile
        confirmVC?.firstName = account?.firstName
        confirmVC?.lastName = account?.lastName
        self.navigationController?.pushViewController(confirmVC!, animated: true)
    }

}
