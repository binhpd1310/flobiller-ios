//
//  ViewController.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/20/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class FLOLoginViewController: BaseViewController {

    @IBOutlet weak var passwordTF: FLOTextField!
    @IBOutlet weak var emailTF: FLOTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setHiddenButtonBack()
        if (DataHelper.isDidLogin()) {
            let storyboard = UIStoryboard.init(name: "Function", bundle: nil)
            let navi  =  storyboard.instantiateViewController(withIdentifier: "mainNavigation")
            let menuView = storyboard.instantiateViewController(withIdentifier: "leftMenu")
            let container = MFSideMenuContainerViewController.container(withCenter: navi, leftMenuViewController: menuView, rightMenuViewController: nil)
            container?.shadow.enabled = false
            container?.menuSlideAnimationEnabled = true
            container?.menuSlideAnimationFactor = 1
            self.present(container!, animated: false, completion: nil)
            AppDelegate.shareIntance().authenToken = UserDefaults.standard.object(forKey: authCodeKey) as? String
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        validateInput()
    }

    func validateInput() {
        emailTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
        if passwordTF.text == BLANK || emailTF.text == BLANK {
            let toast = MDToast.init(text: "All fields must fill", duration: 0.5)
            toast.show()
            return
        }
        if !CommonUltilities.isValidEmail(email: emailTF.text!) {
            let toast = MDToast.init(text: "Email wrong format!", duration: 0.5)
            toast.show()
            return
        }
        if passwordTF.text!.characters.count <= 4 {
            let toast = MDToast.init(text: "Password too short!", duration: 0.5)
            toast.show()
            return
        }


        let input =  LoginInput()
        input.email = emailTF.text

        input.passWord = passwordTF.text

        
        
        APIConnect.login(input) {
            
            DataHelper.didLogin()
            
            if !UserDefaults.standard.bool(forKey: "FLOdidShowWelcome") {
                APIConnect.getImageWelcome(userEmail: self.emailTF.text!, completeBlock: { image in
                    AppDelegate.shareIntance().windowLogin?.hideActivityView()
                    let storyboard = UIStoryboard.init(name: "Function", bundle: nil)
                    let navi  =  storyboard.instantiateViewController(withIdentifier: "mainNavigation")
                    let menuView = storyboard.instantiateViewController(withIdentifier: "leftMenu")
                    let container = MFSideMenuContainerViewController.container(withCenter: navi, leftMenuViewController: menuView, rightMenuViewController: nil)
                    
                    container?.shadow.enabled = false
                    container?.menuSlideAnimationEnabled = true
                    container?.menuSlideAnimationFactor = 1
                    self.present(container!, animated: true, completion: nil)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        let welcomeVC = WelcomeViewController()
                        welcomeVC.image = image
                        navi.present(welcomeVC, animated: true, completion: nil)
                    }
                    

                })
            }else {
                AppDelegate.shareIntance().windowLogin?.hideActivityView()
                let storyboard = UIStoryboard.init(name: "Function", bundle: nil)
                let navi  =  storyboard.instantiateViewController(withIdentifier: "mainNavigation")
                let menuView = storyboard.instantiateViewController(withIdentifier: "leftMenu")
                let container = MFSideMenuContainerViewController.container(withCenter: navi, leftMenuViewController: menuView, rightMenuViewController: nil)
                
                container?.shadow.enabled = false
                container?.menuSlideAnimationEnabled = true
                container?.menuSlideAnimationFactor = 1
                self.present(container!, animated: true, completion: nil)

            }
            
            
        }
    }


}

