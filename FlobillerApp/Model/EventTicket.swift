//
//  EventTicket.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/31/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class EventTicket: NSObject {
    var ticket_id : String?
    var event_id : String?
    var location_id : String?
    var category : String?
    var ticket_date : String?
    var ticket_day : String?
    var ticket_time : String?
    var quantity : String?
    var sold : String?
    var amount : String?
    var etb_amount : String?
    var currency : String?
    var commission: String?

}
