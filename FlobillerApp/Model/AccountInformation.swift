//
//  AccountInformation.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/2/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import Alamofire

class AccountInformation: NSObject {
    var firstName : String?
    var lastName : String?
    var mobile : String?
    var email : String?
    var cust_Id : String?
    var country : String?
    var dateOfBirth : String?
    var countryCode : String?
    var currency : String?

    func objectToParam() -> Parameters {

        return ["email" : self.value(forKey: "email") ?? "",
                "firstname" : self.value(forKey: "firstName") ?? "",
                "lastname" : self.value(forKey: "lastName") ?? "",
                "mobile" : self.value(forKey: "mobile") ?? "",
                "country" : self.value(forKey: "country") ?? "",
                "dateofbirth" : self.value(forKey: "dateOfBirth") ?? "",
                "auth_token" : AppDelegate.shareIntance().authenToken ?? ""]

    }
}
