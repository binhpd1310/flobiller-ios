//
//  ProductOperatorModel.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/21/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class ProductOperatorModel: NSObject {
    var name : String?
    var retailPrice : String?
    var salePrice : String?
    var authentication_key : String?
    var currency : String?
}
