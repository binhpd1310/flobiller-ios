//
//  BillerModel.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/13/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class BillerModel: NSObject {

    var billerId : String?
    var name : String?
    var logo : String?
    var logo_txt : String?
    var country : String?
    var accountNumber : String?

}
