//
//  ContentCategoryModel.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class ContentCategoryModel: NSObject {
    var eventId : String?
    var name : String?
    var amount : String?
    var currency : String?
    var address : String?
    var map_lat : String?
    var map_long : String?
    var city : String?
    var poster : String?
    var image : String?
    var desc : String?
    var short_desc : String?
    var location : String?
}
