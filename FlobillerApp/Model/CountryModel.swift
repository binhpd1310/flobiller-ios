//
//  CountryModel.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class CountryModel: NSObject {
    var key : String?
    var name : String?
    var code : String?
}
