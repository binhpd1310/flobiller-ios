//
//  LoginInput.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import Alamofire

class LoginInput: NSObject {
    var email           : String?
    var googleKey       = "123456"
    var passWord        : String?
    func objectToParam() -> Parameters {
        return ["email" : self.value(forKey: "email") ?? "",
                "password" : self.value(forKey: "passWord") ?? "",
                "google" : self.value(forKey: "googleKey") ?? ""]
    }

}
