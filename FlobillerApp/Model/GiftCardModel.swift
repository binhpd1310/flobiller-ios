//
//  GiftCardModel.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/9/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class GiftCardModel: NSObject {
    var id : String
    var logo : String
    var name : String
    var amountArr : [String]
    
    init(withName name : String?,logo : String?, amountArr : [String], id : String) {
        self.name = name ?? BLANK
        self.logo = (logo != nil) ? ("https://flobiller.flocash.com/assets/images/upload/" + logo!) : BLANK
        self.amountArr = amountArr
        self.id = id
    }
}
