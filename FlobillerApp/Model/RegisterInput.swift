//
//  RegisterInput.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import Alamofire

class RegisterInput: NSObject {

    var email           : String?
    var firstName       : String?
    var lastName        : String?
    var mobile          : String?
    var country         : String?
    var passWord        : String?
    var cPassWord       : String?
    var dob    : String?

    func objectToParam() -> Parameters {
        return ["email" : self.value(forKey: "email") ?? "",
                "firstname" : self.value(forKey: "firstName") ?? "",
                "lastname" : self.value(forKey: "lastName") ?? "",
                "mobile" : self.value(forKey: "mobile") ?? "",
                "country" : self.value(forKey: "country") ?? "",
                "dateofbirth" : self.value(forKey: "dob") ?? "",
                "password" : self.value(forKey: "passWord") ?? "",
                "cpassword" : self.value(forKey: "cPassWord") ?? ""]
    }

}
