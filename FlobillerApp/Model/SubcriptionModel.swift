//
//  SubcriptionModel.swift
//  FlobillerApp
//
//  Created by Lamdq on 8/6/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class UserSubcriptionModel: NSObject {
    var account_name : String?
    var account_number : String?
    var country : String?
    var register_date : String?
    var service_id : String?
    var billername : String?
    var billerlogo : String?
    var billertext : String?
}
