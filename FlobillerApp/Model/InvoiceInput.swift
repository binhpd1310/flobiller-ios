//
//  InvoiceInput.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/16/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import Alamofire

class InvoiceInput: NSObject {
    var auth_token : String?
    var api : String?
    var billerid : String?
    var billerinvoice : String?
    var currency : String?
    var amount : String?
    var country : String?
    var cust_account_name : String?
    var cust_account_number : String?
    var name : String?
    var cust_topup : String?
    var countrycode : String?
    var mobile : String?
    var operator_id : String?
    var giftcardid : String?
    var giftcardname : String?

    func objectToParam() -> Parameters {
        let listPropetiesNames = Mirror(reflecting: self).children.flatMap { $0.label }
        var resultDict = Parameters()
        for property in listPropetiesNames {
            resultDict[property] = self.value(forKey: property) ?? BLANK
        }
        return resultDict
    }
}
