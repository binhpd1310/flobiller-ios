//
//  InvoiceModel.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/3/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class InvoiceModel: NSObject {

    var invoiceId : String?
    var biller_invoiceid : String?
    var name : String?
    var currency : String?
    var amount : String?
    var biller_id : String?
    var service_id : String?
    var generate_date : String?
    var last_pay_date : String?
    var cust_email : String?
    var cust_mobile : String?
    var cust_account_name: String?
    var cust_account_number : String?
    var cust_topup : String?
    var flocash_email : String?
    var flocash_status : String?
    var flocash_trans_id : String?
    var flocash_paid_date : String?
    var api : String?
    var api_status : String?
    var aboOrderID : String?
    var logo : String?

}
