//
//  OperatorModel.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 2/20/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class OperatorModel: NSObject {
    var name : String?
    var operatorId : String?
    var authentication_key : String?
    var logo : String?
    var error_txt : String?

}
