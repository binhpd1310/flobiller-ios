//
//  TicketCategory.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 3/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit

class TicketCategory: NSObject {
    
    var type : String?
    var name : String?
    var logo : String?

}
