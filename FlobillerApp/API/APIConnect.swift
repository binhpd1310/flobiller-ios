//
//  APIConnect.swift
//  FlobillerApp
//
//  Created by Do Quoc Lam on 1/23/17.
//  Copyright © 2017 Do Quoc Lam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON 

class APIConnect: NSObject {

    static func register(_ input: RegisterInput,success successBlock: (() -> Void)?)  {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            return
        }
        AppDelegate.shareIntance().windowLogin?.showActivityView(withLabel: "Loading")
        let request = Alamofire.request("https://flobiller.flocash.com/api/user", method: .post, parameters: input.objectToParam(), encoding: JSONEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in
            AppDelegate.shareIntance().windowLogin?.hideActivityView()
            if response.response?.statusCode == 201 {
                successBlock!()
            }
            if response.response?.statusCode == 400  {
                let toast = MDToast.init(text: "Email has been registered!", duration: 0.5)
                toast.show()
            }
        }
    }

    static func login(_ input: LoginInput,success successBlock: (() -> Void)?)  {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            return
        }
        AppDelegate.shareIntance().windowLogin?.showActivityView(withLabel: "Loading")
        let request = Alamofire.request("https://flobiller.flocash.com/api/login", method: .post, parameters: input.objectToParam(), encoding: JSONEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in
            
            if response.response?.statusCode == 200 {
                if let JSON = response.result.value as? [String: String] {
                    if let authToken = JSON["auth_token"] {
                        AppDelegate.shareIntance().authenToken = authToken
                        UserDefaults.standard.set(authToken, forKey: authCodeKey)
                    }
                }

                successBlock!()
            }
            if response.response?.statusCode == 400  {
                AppDelegate.shareIntance().windowLogin?.hideActivityView()
                let toast = MDToast.init(text: "Password not correct", duration: 0.5)
                toast.show()
            }
        }
    }

    static func forgotPW(_ email: String,success successBlock: (() -> Void)?)  {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            return
        }
        AppDelegate.shareIntance().windowLogin?.showActivityView(withLabel: "Loading")
        let request = Alamofire.request("https://flobiller.flocash.com/api/forget?email=\(email)", method: .get, parameters: nil, encoding: JSONEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in
            AppDelegate.shareIntance().windowLogin?.hideActivityView()
            if response.response?.statusCode == 200 {
                successBlock!()
            }
            if response.response?.statusCode == 404  {
                let toast = MDToast.init(text: "Email does not exist", duration: 0.5)
                toast.show()
            }
        }
    }

    static func getUserInfo(success successBlock: ((_ info : AccountInformation?) -> Void)? , _ failureBlock: (() -> Void)?)  {
        

        let request = Alamofire.request("https://flobiller.flocash.com/api/user?auth_token=\(AppDelegate.shareIntance().authenToken ?? BLANK)", method: .get, parameters: nil, encoding: JSONEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                let info = AccountInformation()
                info.cust_Id = json["user"]["cust_id"].string
                info.email = json["user"]["email"].string
                info.firstName = json["user"]["firstname"].string
                info.lastName = json["user"]["lastname"].string
                info.mobile = json["user"]["mobile"].string
                info.country = json["user"]["country"].string
                info.countryCode = json["user"]["countrycode"].string
                info.currency = json["user"]["currency"].string
                info.dateOfBirth = json["user"]["dateofbirth"].string
                successBlock!(info)
            } else {
                if failureBlock != nil {
                    failureBlock!()
                }
            }
        }
    }

    static func getCountriesUser(_ successBlock: ((_ baseUrl: String?, _ extension: String?) -> Void)?)  {
        if !CheckingNetwork.shareIntance().networkStatus {
            return
        }
        let request = Alamofire.request("https://flobiller.flocash.com/api/usercountries", method: .get, parameters: nil, encoding: JSONEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                successBlock!(json["flag"]["base_url"].string, json["flag"]["extension"].string)
            }
        }
    }

    static func updateUserInfor(_ accountInfo: AccountInformation, success successClosure:(() -> Void)? , failure failureClosure:(() -> Void)?) {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            return
        }
        
        let request = Alamofire.request("https://flobiller.flocash.com/api/user", method: .put, parameters: accountInfo.objectToParam(), encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                successClosure!()
            } else {
                failureClosure!()
            }
        }
    }


    // Get all billers
    static func getAllBillers(success successClosure:((_ billerArray : NSArray ) -> Void)? , failure failureClosure:(() -> Void)?) {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            return
        }

        let request = Alamofire.request("https://flobiller.flocash.com/api/billers?auth_token=\(AppDelegate.shareIntance().authenToken!)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                let arrayBiller = json["billers"].array
                let resultArray = NSMutableArray()
                if arrayBiller != nil {
                    for biller in arrayBiller! {
                        let tBiller = BillerModel()
                        tBiller.billerId = biller["biller_id"].string
                        tBiller.name = biller["name"].string
                        tBiller.logo = biller["logo"].string
                        tBiller.logo_txt = biller["logo_txt"].string
                        tBiller.country = biller["country"].string
                        tBiller.accountNumber = biller["account_number"].string
                        resultArray.add(tBiller)
                        
                    }
                }

                successClosure!(resultArray)
            } else {
                failureClosure!()
            }
            
        }
        
    }

    // Get all billers
    static func getListSubcriptions(_ biller : BillerModel ,success successClosure:((_ subcriptionArray : NSArray ) -> Void)? , failure failureClosure:(() -> Void)?) {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            return
        }

        let request = Alamofire.request("https://flobiller.flocash.com/api/paybillsubscriptions?auth_token=\(AppDelegate.shareIntance().authenToken!)&country_code=\(biller.country ?? BLANK)&biller=\(biller.billerId ?? BLANK)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                let arraySub = json["subscriptions"].array
                let resultArray = NSMutableArray()
                if arraySub != nil {
                    for sub in arraySub! {
                        let tSub = SubcriptionModel()
                        tSub.name = sub["value"].string
                        tSub.id = sub["key"].string

                        resultArray.add(tSub)

                    }
                }

                successClosure!(resultArray)
            } else {
                failureClosure!()
            }

        }
        
    }

    //Create invoice
    static func createInvoice(_ input: InvoiceInput,success successBlock: ((_ invoiceId : String?) -> Void)?, failure failureBlock: ((_ message : String?) -> Void)?)  {

        let request = Alamofire.request("https://flobiller.flocash.com/api/invoice", method: .post, parameters: input.objectToParam(), encoding: JSONEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in
            let json = JSON(response.result.value ?? BLANK)
            if response.response?.statusCode == 200 {
                let idInvoice = json["invoice"]["invoice"].double
                successBlock!( String(format:"%.0f", idInvoice!))
            } else {

                let msg = json["msg"].string

                failureBlock!(msg)
            }
        }
    }
    // Get all operators country
    static func getAllOperators(_ countryCode : String ,success successClosure:((_ subcriptionArray : NSArray ) -> Void)? , failure failureClosure:(() -> Void)?) {


        let request = Alamofire.request("https://flobiller.flocash.com/api/airtimeoperators?auth_token=\(AppDelegate.shareIntance().authenToken!)&ciso=\(countryCode)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                if json["operators"]["error_code"].string == "0" {
                    let operatorString = json["operators"]["operator"].string
                    let operatorArray = operatorString?.components(separatedBy: ",")

                    let operatorIdString = json["operators"]["operatorid"].string
                    let operatorIdArray = operatorIdString?.components(separatedBy: ",")
                    let authentication_key = json["operators"]["authentication_key"].string

                    var logo = json["operators"]["logo"].string ?? BLANK
                    if logo.contains("{ID}") {
                        logo = logo.replacingOccurrences(of: "{ID}", with: operatorIdArray?[0] ?? BLANK)
                    }

                    let operatorResultArray = NSMutableArray()
                    if operatorIdArray == nil {
                        successClosure!(operatorResultArray)
                        return
                    }
                    if operatorIdArray!.count < 1 {
                        successClosure!(operatorResultArray)
                        return
                    }
                    for i in 0...(operatorIdArray!.count - 1) {
                        let tmp = OperatorModel()
                        tmp.authentication_key = authentication_key
                        tmp.name = operatorArray?[i]
                        tmp.operatorId = operatorIdArray?[i]
                        tmp.logo = logo
                        operatorResultArray.add(tmp)
                    }
                    successClosure!(operatorResultArray)

                } else {
                    failureClosure!()

                }
            } else {
                failureClosure!()
            }
            
        }
        
    }


    // Get all airtime topups
    static func getAllAirtimeTopups(_ operatorId : String ,success successClosure:((_ productArray : NSArray ) -> Void)? , failure failureClosure:(() -> Void)?) {


        let request = Alamofire.request("https://flobiller.flocash.com/api/airtimetopup?auth_token=\(AppDelegate.shareIntance().authenToken!)&operator=\(operatorId)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                if json["topups"]["error_code"].string == "0" {
                    let productListString = json["topups"]["product_list"].string
                    let productList = productListString?.components(separatedBy: ",")

                    let retailPriceString = json["topups"]["retail_price_list"].string
                    let retailPriceArray = retailPriceString?.components(separatedBy: ",")

                    let salePriceString = json["topups"]["wholesale_price_list"].string
                    let salePriceArray = salePriceString?.components(separatedBy: ",")

                    let authentication_key = json["topups"]["authentication_key"].string
                    let currency = json["topups"]["destination_currency"].string


                    let productArray = NSMutableArray()
                    if productList == nil {
                        successClosure!(productArray)
                        return
                    }
                    if productList!.count < 1 {
                        successClosure!(productArray)
                        return
                    }
                    for i in 0...(productList!.count - 1) {
                        let tmp = ProductOperatorModel()
                        tmp.authentication_key = authentication_key
                        tmp.name = productList?[i]
                        tmp.retailPrice = retailPriceArray?[i]
                        tmp.salePrice = salePriceArray?[i]
                        tmp.currency = currency
                        productArray.add(tmp)
                    }
                    successClosure!(productArray)

                } else {
                    failureClosure!()

                }
            } else {
                failureClosure!()
            }
            
        }
        
    }
    static func getAllInvoices(_ serviceId: String, successClosure:((_ productArray : NSArray ) -> Void)? , failure failureClosure:(() -> Void)?) {


        let request = Alamofire.request("https://flobiller.flocash.com/api/orders?auth_token=\(AppDelegate.shareIntance().authenToken!)&service=\(serviceId)&start=0", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                let arrayInvoice = json["orders"].array
                if arrayInvoice == nil {
                    failureClosure!()
                    return
                }
                let resultArr = NSMutableArray()
                for invoiceJson in arrayInvoice! {
                    let tmp = InvoiceModel()
                    tmp.invoiceId = invoiceJson["invoice_id"].string
                    tmp.biller_id = invoiceJson["biller_id"].string
                    tmp.name = invoiceJson["name"].string
                    tmp.currency = invoiceJson["currency"].string
                    tmp.amount = invoiceJson["amount"].string
                    tmp.biller_invoiceid = invoiceJson["biller_invoiceid"].string
                    tmp.service_id = invoiceJson["service_id"].string
                    tmp.generate_date = invoiceJson["generate_date"].string
                    tmp.last_pay_date = invoiceJson["last_pay_date"].string
                    tmp.cust_email = invoiceJson["cust_email"].string
                    tmp.cust_mobile = invoiceJson["cust_mobile"].string
                    tmp.cust_account_name = invoiceJson["cust_account_name"].string
                    tmp.cust_account_number = invoiceJson["cust_account_number"].string
                    tmp.cust_topup = invoiceJson["cust_topup"].string
                    tmp.flocash_email = invoiceJson["flocash_email"].string
                    tmp.flocash_status = invoiceJson["flocash_status"].string
                    tmp.flocash_trans_id = invoiceJson["flocash_trans_id"].string
                    tmp.flocash_paid_date = invoiceJson["flocash_paid_date"].string
                    tmp.api = invoiceJson["api"].string
                    tmp.api_status = invoiceJson["api_status"].string
                    tmp.aboOrderID = invoiceJson["aboOrderID"].string
                    tmp.logo = invoiceJson["logo"].string
                    resultArr.add(tmp)
                }

                successClosure!(resultArr)
            } else {
                failureClosure!()
            }
            
        }
        
    }


    static func markAsPaid(_ orderId: String , successClosure:((_ msg : String) -> Void)? , failure failureClosure:(() -> Void)?) {

        let param = ["orderid" :  orderId,
                     "auth_token" : AppDelegate.shareIntance().authenToken!]


        let request = Alamofire.request("https://flobiller.flocash.com/api/notify", method:.post, parameters: param, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)

                successClosure!(json["msg"].string ?? BLANK)
                
            } else {
                failureClosure!()
            }

        }
        
    }

    static func getAllGiftCard(_ successClosure:((_ arr : [GiftCardModel]) -> Void)? , failure failureClosure:(() -> Void)?) {

        let request = Alamofire.request("https://flobiller.flocash.com/api/giftcards?auth_token=\(AppDelegate.shareIntance().authenToken!)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                var arrResult  = [GiftCardModel]()
                if let arrJsonGift = json["giftcards"].array {
                    for json in arrJsonGift {
                        let amountArr = json["amount"].string?.components(separatedBy: "|")

                        let giftModel = GiftCardModel.init(withName: json["name"].string, logo: json["logo"].string, amountArr: amountArr ?? [String](),id: json["giftcard"].string ?? BLANK)
                        arrResult.append(giftModel)
                    }
                }
                successClosure?(arrResult)
            } else {
                failureClosure!()
            }
            
        }
        
    }

    static func getInvoicesByOrderId(_ invoiceId : String , successClosure:((_ invoiceModel : InvoiceModel ) -> Void)? , failure failureClosure:(() -> Void)?) {


        let request = Alamofire.request("https://flobiller.flocash.com/api/order?auth_token=\(AppDelegate.shareIntance().authenToken!)&orderid=\(invoiceId)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                let invoiceJson = json["order"]["invoice"]
                let tmp = InvoiceModel()
                tmp.invoiceId = invoiceJson["invoice_id"].string
                tmp.biller_id = invoiceJson["biller_id"].string
                tmp.name = invoiceJson["name"].string
                tmp.currency = invoiceJson["currency"].string
                tmp.amount = invoiceJson["amount"].string
                tmp.biller_invoiceid = invoiceJson["biller_invoiceid"].string
                tmp.service_id = invoiceJson["service_id"].string
                tmp.generate_date = invoiceJson["generate_date"].string
                tmp.last_pay_date = invoiceJson["last_pay_date"].string
                tmp.cust_email = invoiceJson["cust_email"].string
                tmp.cust_mobile = invoiceJson["cust_mobile"].string
                tmp.cust_account_name = invoiceJson["cust_account_name"].string
                tmp.cust_account_number = invoiceJson["cust_account_number"].string
                tmp.cust_topup = invoiceJson["cust_topup"].string
                tmp.flocash_email = invoiceJson["flocash_email"].string
                tmp.flocash_status = invoiceJson["flocash_status"].string
                tmp.flocash_trans_id = invoiceJson["flocash_trans_id"].string
                tmp.flocash_paid_date = invoiceJson["flocash_paid_date"].string
                tmp.api = invoiceJson["api"].string
                tmp.api_status = invoiceJson["api_status"].string
                tmp.aboOrderID = invoiceJson["aboOrderID"].string
                tmp.logo = invoiceJson["logo"].string

                successClosure!(tmp)
            } else {
                failureClosure!()
            }
            
        }
        
    }

    static func getTicketCatergories(_ successClosure:((_ array : [TicketCategory] ) -> Void)? , failure failureClosure:(() -> Void)?) {

        let request = Alamofire.request("https://flobiller.flocash.com/api/eventcategory?auth_token=\(AppDelegate.shareIntance().authenToken!)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in
            var resultArray = [TicketCategory]()
            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                if let arrayJson = json["msg"].array {
                    for categoryJson in arrayJson {
                        let tmp = TicketCategory()
                        tmp.name = categoryJson["name"].string
                        tmp.logo = categoryJson["logo"].string
                        tmp.type = categoryJson["type"].string
                        resultArray.append(tmp)
                    }
                }
                successClosure!(resultArray)
            } else {
                failureClosure!()
            }
            
        }
        
    }

    static func getContentCategories(_ eventType : String, country : String , successClosure:((_ array : [ContentCategoryModel] ) -> Void)? , failure failureClosure:(() -> Void)?) {

        let request = Alamofire.request("https://flobiller.flocash.com/api/eventtype?auth_token=\(AppDelegate.shareIntance().authenToken!)&type=\(eventType)&country=\(country)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in
            var resultArray = [ContentCategoryModel]()
            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                if let arrayJson = json["msg"].array {
                    for categoryJson in arrayJson {
                        let tmp = ContentCategoryModel()
                        tmp.eventId = categoryJson["event_id"].string
                        tmp.name = categoryJson["name"].string
                        tmp.amount = categoryJson["amount"].string
                        tmp.currency = categoryJson["currency"].string
                        tmp.address = categoryJson["address"].string

                        tmp.map_lat = categoryJson["map_lat"].string
                        tmp.map_long = categoryJson["map_long"].string
                        tmp.city = categoryJson["city"].string
                        tmp.poster = categoryJson["poster"].string
                        tmp.image = categoryJson["image"].string
                        tmp.desc = categoryJson["desc"].string
                        tmp.short_desc = categoryJson["short_desc"].string
                        tmp.location = categoryJson["location"].string
                        resultArray.append(tmp)
                    }
                }
                successClosure!(resultArray)
            } else {
                failureClosure!()
            }

        }
        
    }

    static func getEventId(_ eventId : String, successClosure:((_ array : [EventTicket] ) -> Void)? , failure failureClosure:(() -> Void)?) {

        let request = Alamofire.request("https://flobiller.flocash.com/api/eventid?auth_token=\(AppDelegate.shareIntance().authenToken!)&id=\(eventId)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in
            var resultArray = [EventTicket]()
            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                if let arrayJson = json["msg"]["eventticketdata"].array {
                    for categoryJson in arrayJson {
                        let ticketData = EventTicket()
                        let listPropetiesNames = Mirror(reflecting: ticketData).children.flatMap { $0.label }

                        for property in listPropetiesNames {
                            if property == "ticket_day" || property == "ticket_time" {
                                continue
                            }

                            ticketData.setValue(categoryJson[property].string, forKey: property)
                        }
                        if ticketData.ticket_date != nil {
                            ticketData.ticket_day  = ticketData.ticket_date!.components(separatedBy: " ")[0]
                            ticketData.ticket_time = ticketData.ticket_date!.components(separatedBy: " ")[1]
                        }
                        if ticketData.quantity != "0" {
                            resultArray.append(ticketData)
                        }

                    }

                }

                successClosure!(resultArray)
            } else {
                failureClosure!()
            }
            
        }
        

    }

    static func eventQuantity(_ eventId: String , successClosure:(() -> Void)? , failure failureClosure:(() -> Void)?) {

        let param = ["id" :  eventId,
                     "auth_token" : AppDelegate.shareIntance().authenToken!]


        let request = Alamofire.request("https://flobiller.flocash.com/api/eventqty", method:.post, parameters: param, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                if json["status"].string == "success" {
                    successClosure!()
                } else {
                    failureClosure!()
                }
            } else {
                failureClosure!()
            }
            
        }
        
    }

    static func eventAdd(_ ticket : EventTicket,contentDetail : ContentCategoryModel ,userInfo : AccountInformation ,quantity : String, amount : String, successClosure:((_ message : String) -> Void)? , failure failureClosure:(() -> Void)?) {

        let param = ["event_id" :  contentDetail.eventId ?? BLANK,
                     "auth_token" : AppDelegate.shareIntance().authenToken!,
                     "ticket_id" : ticket.ticket_id ?? BLANK,
                     "eventname" : contentDetail.name ?? BLANK,
                     "eventcountries" : contentDetail.city ?? BLANK,
                     "eventemail" : userInfo.email ?? BLANK,
                     "eventmobile" : userInfo.mobile ?? BLANK,
                     "selectqty" : quantity,
                     "eventcurrency" : ticket.currency ?? BLANK,
                     "eventamount" : amount]


        let request = Alamofire.request("https://flobiller.flocash.com/api/eventadd", method:.post, parameters: param, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")

        request.responseJSON { response in

            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                if json["status"].string == "success" {
                    successClosure!("\(json["msg"]["localinvoice"].number ?? 0)")
                } else {
                    failureClosure!()
                }
            } else {
                failureClosure!()
            }
            
        }
        
    }

    //
    static func getUserSubcriptions(success successClosure:((_ subcriptionArray : NSArray ) -> Void)? , failure failureClosure:(() -> Void)?) {
        if !CheckingNetwork.shareIntance().networkStatus {
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            failureClosure!()
            return
        }
        
        let request = Alamofire.request("https://flobiller.flocash.com/api/subscription?auth_token=\(AppDelegate.shareIntance().authenToken!)", method:.get, parameters: nil, encoding: URLEncoding.default).authenticate(user: "flobiller", password: "flo@12345")
        
        request.responseJSON { response in
            
            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                let arraySub = json["subscriptions"].array
                let resultArray = NSMutableArray()
                if arraySub != nil {
                    for sub in arraySub! {
                        let tSub = UserSubcriptionModel()
                        tSub.account_name = sub["account_name"].string
                        tSub.account_number = sub["account_number"].string
                        tSub.country = sub["country"].string
                        tSub.register_date = sub["register_date"].string
                        tSub.service_id = sub["service_id"].string
                        tSub.billername = sub["billername"].string
                        tSub.billerlogo = sub["billerlogo"].string
                        tSub.billertext = sub["billertext"].string
                        
                        resultArray.add(tSub)
                        
                    }
                }
                
                successClosure!(resultArray)
            } else {
                failureClosure!()
            }
            
        }
        
    }
    
    static func deleteUserSubscription(serviceId : String ,success successClosure:(() -> Void)? , failure failureClosure:(() -> Void)?) {
        if !CheckingNetwork.shareIntance().networkStatus {
            failureClosure!()
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            return
        }
        
        let param = ["id" :  serviceId,
                     "auth_token" : AppDelegate.shareIntance().authenToken!]
        
        let request = Alamofire.request("https://flobiller.flocash.com/api/subscription", method: .delete
            , parameters: param, encoding: URLEncoding.httpBody, headers: nil)
        
        request.responseJSON { response in
            
            if response.response?.statusCode == 200 {
                successClosure!()
            } else {
                failureClosure!()
            }
            
        }
        
    }
    static func updateSubscription(serviceId : String ,accname : String, accno : String,success successClosure:(() -> Void)? , failure failureClosure:(() -> Void)?) {
        if !CheckingNetwork.shareIntance().networkStatus {
            failureClosure!()
            let toast = MDToast.init(text: "No Internet connection", duration: 0.5)
            toast.show()
            return
        }
        
        let param = ["id" :  serviceId,
                     "auth_token" : AppDelegate.shareIntance().authenToken!,
                     "accname" : accname,
                     "accno" : accno]
        
        let request = Alamofire.request("https://flobiller.flocash.com/api/subscription", method: .put
            , parameters: param, encoding: URLEncoding.default, headers: nil)
        
        request.responseJSON { response in
            
            if response.response?.statusCode == 200 {
                successClosure!()
            } else {
                failureClosure!()
            }
            
        }
        
    }
    static func getImageWelcome(userEmail: String, completeBlock:@escaping ((UIImage?) -> Void)){
        
        let request = Alamofire.request("https://flobiller.flocash.com/api/promo?auth_token=\(AppDelegate.shareIntance().authenToken!)&email=\(userEmail)", method: .get, parameters: nil, encoding: URLEncoding.httpBody).authenticate(user: "flobiller", password: "flo@12345")
        request.responseJSON { response in
            
            if response.response?.statusCode == 200 {
                let json = JSON(response.result.value ?? BLANK)
                guard let baseUrl = json["banner"]["base_url"].string, let path = json["path"].string,let extension_ = json["banner"]["extension"].string else {
                    completeBlock(nil)
                    return
                    
                }
                
                let urlLink = baseUrl + path + "." + extension_
                if let url = URL.init(string: urlLink) {
                    let data = try! Data.init(contentsOf: url)
                    
                    let image = UIImage.init(data: data)
                    
                    completeBlock(image)
                } else {
                    completeBlock(nil)
                }
                
            } else {
                completeBlock(nil)
            }
            
        }

    }


}
